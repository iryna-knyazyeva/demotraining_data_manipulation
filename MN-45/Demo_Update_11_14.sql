----------------------------------------------------------------------
 -----------------------STEP 1 RESTORE DOMAIN EVENTS-------------------
 ----------------------------------------------------------------------
 ------------------------------------------------------------------------------
 -------------------BACK UP RESTORE NEWS_FEED_EVENTS_TBL-----------------------
 ------------------------------------------------------------------------------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		1,
		'Transitions in LOC',
		'ed-admission',
		'2018-11-16-01.01.35.015564',
		'1000000000',
		'SIMON',
		'DeALPHA',
		'1000000000',
		'ADT'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2,
		'Transitions in LOC',
		'ed-admission',
		'2018-11-16-01.01.35.015564',
		'1000000001',
		'JOHN',
		'WALKER',
		'1000000001',
		'ADT'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		3,
		'Transitions in LOC',
		'ip-prior-auth',
		'2018-11-15-22.24.39.591360',
		'1000000003',
		'AARON',
		'DeTINIOS',
		'1000000003',
		'Prior Authorization'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		4,
		'Transitions in LOC',
		'ip-prior-auth',
		'2018-11-15-22.24.39.591360',
		'1000000005',
		'DEBRA',
		'SABETES',
		'1000000005',
		'Prior Authorization'
	);

-------------------------------------------------------------------------------------
 -------------------BACK UP RESTORE NEWS_FEED_EVENT_DETAILS_TBL-----------------------
 -------------------------------------------------------------------------------------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'admitted-on',
		'2018-11-15'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'event-description',
		'ADMISSION'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'event-location',
		'EMERGENCY'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'hospital-name',
		'JAMES HOSPITAL'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'admitted-on',
		'2018-11-15'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'event-description',
		'ADMISSION'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'event-location',
		'EMERGENCY'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'hospital-name',
		'JOHN HOSPITAL'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'facility-name',
		'Fonda medical center'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'service-on',
		'2018-11-14'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'service-type',
		'INPATIENT BH'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'facility-name',
		'Fonda medical center'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'service-on',
		'2018-11-13'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'service-type',
		'Medical'
	);


 UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	HH_NAME = 'Community Health Home'
WHERE
	MBR_DK IN(
		1000000000,
		1000000001,
		1000000002,
		1000000003,
		1000000004,
		1000000005
	);


UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	HH_NAME = 'Hunter Valley Health Home'
WHERE
	MBR_DK IN(
		13882,
		16683,
		31351,
		31369,
		31572,
		32110,
		32338,
		32983,
		33431,
		34521
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	HH_NAME = 'Compass Health Home'
WHERE
	MBR_DK IN(
		34580,
		34614,
		34840,
		34930,
		35322,
		35662,
		35812,
		36231,
		36302,
		36520
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	HH_NAME = 'Integrated Care Health Home'
WHERE
	MBR_DK IN(
		36802,
		37196,
		37252,
		37550,
		37949,
		38043,
		38220,
		39437,
		39634,
		39800
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	HH_NAME = 'Coordinated Care Health Home'
WHERE
	MBR_DK IN(
		40449,
		40711,
		40845,
		41016,
		44443,
		44947,
		45015,
		47147,
		47448,
		47466
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	HH_NAME = 'Wellness Health Home'
WHERE
	MBR_DK IN(
		49442,
		51061,
		52674,
		53955,
		57892,
		58059,
		58253,
		59387,
		62526,
		61925
	);


DELETE
FROM
	MENDATA.MEMBER_LIST_TBL
WHERE
	MBR_DK IN(
		40,
		64,
		118,
		168,
		170,
		204,
		210,
		227,
		255,
		284,
		291,
		309,
		317,
		324,
		363,
		374,
		406,
		409,
		410,
		413,
		452,
		453,
		457,
		460,
		468,
		470,
		487,
		489,
		512,
		513,
		515,
		517,
		531,
		536,
		542,
		20000025,
		20000029,
		20000030,
		20000031,
		20000188,
		20000178
	);



---------------FOR risk score-----------



UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	SOCIAL_DETERM_RISK_SCORE = 9.0,
	SOCIAL_SUPPORT = 'Yes'
WHERE
	MBR_DK = '1000000005';
	
--------------for Reset Cache----------------

CALL MENDATA.RESET_APPLICATION_CACHE();

