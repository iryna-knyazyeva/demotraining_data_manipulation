-----------UI TABLE UPDATE----------------
 ----------------FOR NEWS FEED UPDATE------------------
 ----------------------------FOR MBR_DK='1000000000' ADT 'ed-admission' --------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		1,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'1000000000',
		'SIMON',
		'DeALPHA',
		'1000000000',
		'ADT'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'admitted-on',
		'2018-10-08'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'hospital-name',
		'JAMES HOSPITAL'
	);

------------FOR MBR_DK='1000000001' ADT 'ed-admission' --------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'1000000001',
		'JOHN',
		'WALKER',
		'1000000001',
		'ADT'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'admitted-on',
		'2018-10-08'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'hospital-name',
		'JOHN HOSPITAL'
	);

----------------------------FOR MBR_DK='1000000003'  'ip-prior-auth' --------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL (
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		3,
		'Transitions in LOC',
		'ip-prior-auth',
		'2018-10-10-00.00.00.000000',
		'1000000003',
		'AARON',
		'DeTINIOS',
		'1000000003',
		'Prior Authorization'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'facility-name',
		'Fonda medical center'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'service-on',
		'2018-10-07'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'service-type',
		'INPATIENT BH'
	);

----------------------------FOR MBR_DK='1000000005'  'ip-prior-auth' --------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL (
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		4,
		'Transitions in LOC',
		'ip-prior-auth',
		'2018-10-10-00.00.00.000000',
		'1000000005',
		'DEBRA',
		'SABETES',
		'1000000005',
		'Prior Authorization'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'facility-name',
		'Fonda medical center'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'service-on',
		'2018-10-06'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'service-type',
		'Medical'
	);

------------------------------------FOR MESSAGE UPDATE--------------------	
 UPDATE
	MENDATA.NOTICE_TBL
SET
	SHORT_MESSAGE = 'IP Discharge  occurred on 10-09-2018' ,
	DETAILED_MESSAGE = 'IP Discharge  occurred on 10-09-2018 and no follow up visit has occurred'
WHERE
	mbr_dk = '1000000005'
	AND ntfy_fct_dk = '981604';

 UPDATE
	MENDATA.NOTICE_TBL
SET
	SHORT_MESSAGE = 'Fonda medical center  - E1169 10-06-2018 - 10-09-2018' ,
	DETAILED_MESSAGE = 'Prior Authorization for 99221 from 10-06-2018 to 10-09-2018 for treatment of E1169'
WHERE
	mbr_dk = '1000000005'
	AND ntfy_fct_dk = '2816';


 CALL MENDATA.RESET_APPLICATION_CACHE();

 COMMIT;
