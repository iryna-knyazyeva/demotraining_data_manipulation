
-----------UI TABLE UPDATE----------------
 ----------------FOR NEWS FEED UPDATE------------------
 ----------------------------FOR MBR_DK='1000000000' ADT 'ed-admission' --------
-----------UI TABLE UPDATE----------------
 ----------------FOR NEWS FEED UPDATE------------------
 ----------------------------FOR MBR_DK='1000000000' ADT 'ed-admission' --------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		1,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'1000000000',
		'SIMON',
		'DeALPHA',
		'1000000000',
		'ADT'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'admitted-on',
		'2018-10-08'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		1,
		'hospital-name',
		'JAMES HOSPITAL'
	);

------------FOR MBR_DK='1000000001' ADT 'ed-admission' --------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'1000000001',
		'JOHN',
		'WALKER',
		'1000000001',
		'ADT'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'admitted-on',
		'2018-10-08'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2,
		'hospital-name',
		'JOHN HOSPITAL'
	);

----------------------------FOR MBR_DK='1000000003'  'ip-prior-auth' --------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL (
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		3,
		'Transitions in LOC',
		'ip-prior-auth',
		'2018-10-10-00.00.00.000000',
		'1000000003',
		'AARON',
		'DeTINIOS',
		'1000000003',
		'Prior Authorization'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'facility-name',
		'Fonda medical center'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'service-on',
		'2018-10-07'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		3,
		'service-type',
		'INPATIENT BH'
	);

----------------------------FOR MBR_DK='1000000005'  'ip-prior-auth' --------
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL (
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		4,
		'Transitions in LOC',
		'ip-prior-auth',
		'2018-10-10-00.00.00.000000',
		'1000000005',
		'DEBRA',
		'SABETES',
		'1000000005',
		'Prior Authorization'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'facility-name',
		'Fonda medical center'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'service-on',
		'2018-10-06'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		4,
		'service-type',
		'Medical'
	);

------------------------------------FOR MESSAGE UPDATE--------------------	
 UPDATE
	MENDATA.NOTICE_TBL
SET
	SHORT_MESSAGE = 'IP Discharge  occurred on 10-09-2018' ,
	DETAILED_MESSAGE = 'IP Discharge  occurred on 10-09-2018 and no follow up visit has occurred'
WHERE
	mbr_dk = '1000000005'
	AND ntfy_fct_dk = '981604';

 UPDATE
	MENDATA.NOTICE_TBL
SET
	SHORT_MESSAGE = 'Fonda medical center  - E1169 10-06-2018 - 10-09-2018' ,
	DETAILED_MESSAGE = 'Prior Authorization for 99221 from 10-06-2018 to 10-09-2018 for treatment of E1169'
WHERE
	mbr_dk = '1000000005'
	AND ntfy_fct_dk = '2816';


 -----------for MBR_DK 1000000005 TCP RED LINE ISSUE --------------
 UPDATE
	MENDATA.MBR_DAY_FOLLOWUP_FCT
SET
	DISCHARGE_DATE = '2018-10-09'
WHERE
	MEASURE_ID = '398538'
	AND MEMBER_ID = '1000000005';

-----------UI TABLE OLD UPDATE----------------
 DELETE
FROM
	MENDATA.PRESCRIPTION_TBL
WHERE
	MBR_DK IN(
		'40',
		'64',
		'118',
		'168',
		'170',
		'204',
		'210',
		'227',
		'255',
		'284',
		'291',
		'309',
		'317',
		'324',
		'363',
		'374',
		'406',
		'409',
		'410',
		'413',
		'452',
		'453',
		'457',
		'460',
		'468',
		'470',
		'487',
		'489',
		'512',
		'513',
		'515',
		'517',
		'531',
		'536',
		'542'
	)
	AND PRESCRIPTION_START_DATE >= '2018-05-01'
	AND DRUG_CLASS = 'Antipsychotics'
	AND CLM_PRSCPTN_DK <= '2001350602';

 UPDATE
	MENDATA.PRESCRIPTION_TBL
SET
	DATE_FILLED = PRESCRIPTION_START_DATE
WHERE
	MBR_DK IN(
		'1000000000',
		'1000000001',
		'1000000002',
		'1000000003',
		'1000000004',
		'1000000005'
	)
	AND PRESCRIPTION_START_DATE > DATE_FILLED;

----------------------------------------------------------------------------------
 -------------Master Query Demo for changing dates correctly-----------------------
 ----------------------------------------------------------------------------------
 MERGE INTO
	MENDATA.PRESCRIPTION_TBL MLT
		USING(
		SELECT
			PRSCPTN_START_DT,
			CLM_PRSCPTN_DK
		FROM
			MENDATA.CLM_PRSCPTN_FCT
	) AS MF ON
	MLT.CLM_PRSCPTN_DK = MF.CLM_PRSCPTN_DK
	AND MLT.MBR_DK IN(
		'1000000000',
		'1000000003'
	)
	WHEN MATCHED THEN UPDATE
	SET
		MLT.PRESCRIPTION_START_DATE = MF.PRSCPTN_START_DT,
		MLT.DATE_FILLED = MF.PRSCPTN_START_DT;

 UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	SOCIAL_DETERM_RISK_SCORE = 9.0,
	SOCIAL_SUPPORT = 'Yes'
WHERE
	MBR_DK = '1000000005';

 CALL MENDATA.RESET_APPLICATION_CACHE();

 COMMIT;
