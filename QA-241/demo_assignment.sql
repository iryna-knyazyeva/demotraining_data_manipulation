-----------------ATTACHING MEMBERS----------------------------
 ------------------------for testuser1-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser1-test',
	CC_EMP_ID = 'testuser1-test1'
WHERE
	MBR_DK IN(
		11521,
		11608,
		11683,
		12238,
		13272,
		13277,
		63160,
		63161
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser2-test',
	CC_EMP_ID = 'testuser2-test1'
WHERE
	MBR_DK IN(
		14957,
		15646,
		15795,
		16686,
		17503,
		17503,
		17519,
		18158,
		18401,
		63164,
		63165
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser3-test',
	CC_EMP_ID = 'testuser3-test1'
WHERE
	MBR_DK IN(
		18668,
		18734,
		20072,
		26060,
		26648,
		26895,
		27463,
		27623,
		27937,
		28704,
		63168
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser7-test',
	CC_EMP_ID = 'testuser7-test1'
WHERE
	MBR_DK IN(
		29827,
		30302,
		30767,
		32386,
		32575,
		33008,
		33531,
		34023,
		63169
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser8-test',
	CC_EMP_ID = 'testuser8-test1'
WHERE
	MBR_DK IN(
		36118,
		36313,
		36590,
		38402,
		39481,
		41730,
		42818,
		63171,
		63172
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser9-test',
	CC_EMP_ID = 'testuser9-test1'
WHERE
	MBR_DK IN(
		44294,
		45424,
		46413,
		47682,
		48391,
		48717,
		49633,
		49904,
		63173,
		63174
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser10-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser10-test',
	CC_EMP_ID = 'testuser10-test1'
WHERE
	MBR_DK IN(
		49950,
		52306,
		52401,
		52897,
		53849,
		54226,
		55142,
		55876,
		58269,
		63175,
		63178
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser11-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser11-test',
	CC_EMP_ID = 'testuser11-test1'
WHERE
	MBR_DK IN(
		58797,
		65445,
		65575,
		65683,
		66150,
		66425,
		66443,
		66572,
		66757,
		63179,
		63180
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser12-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser12-test',
	CC_EMP_ID = 'testuser12-test1'
WHERE
	MBR_DK IN(
		67287,
		67288,
		67289,
		67290,
		67291,
		67292,
		67293,
		67294,
		67295,
		67296,
		67297,
		63170
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for spleshakov-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'spleshakov-test',
	CC_EMP_ID = 'spleshakovtest1'
WHERE
	MBR_DK IN(
		67298,
		67299,
		67300,
		67302,
		67303,
		67304,
		67306,
		67307,
		67308,
		67309,
		63166
	)
	AND ENROLL_STS_CD = 'Active';


SELECT TRIGGERED_ON_TS FROM MENDATA.NEWS_FEED_EVENTS_TBL_1 WHERE TRIGGERED_ON_TS LIKE '%2018%'


UPDATE MENDATA.NEWS_FEED_EVENTS_TBL_STG SET  TRIGGERED_ON_TS ='2018-08-29-16.42.28.000000' WHERE 1=1;



SELECT
	DISTINCT p.MBR_DK,
	p.CC_FULL_NM,
	p.CC_EMP_ID,
	e.TRIGGERED_ON_TS
FROM
	MENDATA.NEWS_FEED_EVENTS_TBL e
INNER JOIN MENDATA.MBR_PROG_TBL p ON
	p.MBR_DK = e.MBR_DK
WHERE
	p.enroll_sts_cd = 'Active'
	AND e.EVENT_TYPE = 'ed-admission' AND p.CC_FULL_NM  IN  ('iknyazyeva')--AND p.CC_FULL_NM NOT LIKE '%testuser%' --AND e.EVENT_TYPE = 'ip-prior-auth'
ORDER BY
	e.TRIGGERED_ON_TS DESC;
	
SELECT * FROM MENDATA.NEWS_FEED_EVENTS_TBL_1 WHERE EVENT_TYPE='ip-prior-auth';



UPDATE MENDATA.MBR_PROG_TBL SET CC_FULL_NM ='iknyazyeva' , CC_EMP_ID='iknyazyeva1' WHERE MBR_DK IN ('63163');


CALL MENDATA.RESET_APPLICATION_CACHE();

SELECT ACTIVE FROM MENDATA.MBR_DIM WHERE MBR_DK='63163';