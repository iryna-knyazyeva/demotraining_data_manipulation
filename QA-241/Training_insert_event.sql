-----------UI TABLE UPDATE----------------
 ----------------FOR NEWS FEED UPDATE------------------
 ----------------------------FOR MBR_DK='27463' ADT 'ed-admission' --------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2188,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'27463',
		'AARON',
		'ALIU',
		'210000070',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2188,
		'admitted-on',
		'2017-03-11'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2188,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2188,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2188,
		'hospital-name',
		'JAMES HOSPITAL'
	);
------------------------------------------66150	2017-03-11-03.03.25.033415	210002667	Alessandro	Rivera	----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2189,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'66150',
		'Alessandro',
		'Rivera',
		'210002667',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2189,
		'admitted-on',
		'2017-03-11'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2189,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2189,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2189,
		'hospital-name',
		'JAMES HOSPITAL'
	);
------------------------------------------44294	2017-03-10-21.56.25.033415	210003080	AARON	CARR		----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2190,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'44294',
		'AARON',
		'CARR',
		'210003080',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2190,
		'admitted-on',
		'2017-03-10'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2190,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2190,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2190,
		'hospital-name',
		'JAMES HOSPITAL'
	);
------------------------------------------49633	2017-03-10-04.27.25.033415	210004037	ABDUL	RIZVI	----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2191,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'49633',
		'ABDUL',
		'RIZVI',
		'210004037',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2191,
		'admitted-on',
		'2017-03-10'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2191,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2191,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2191,
		'hospital-name',
		'JAMES HOSPITAL'
	);
------------------------------------------71127	2017-08-16-18.50.00.000000	37	Leilani	Jefferson	MEDICAL CENTER OF TRINITY----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2192,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'71127',
		'Leilani',
		'Jefferson',
		'37',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2192,
		'admitted-on',
		'2017-08-16'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2192,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2192,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2192,
		'hospital-name',
		'MEDICAL CENTER OF TRINITY'
	);
------------------------------------------71127	2017-08-16-18.50.00.000000	37	Leilani	Jefferson	MEDICAL CENTER OF TRINITY----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2193,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'71127',
		'Leilani',
		'Jefferson',
		'37',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2193,
		'admitted-on',
		'2017-08-16'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2193,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2193,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2193,
		'hospital-name',
		'MEDICAL CENTER OF TRINITY'
	);

	------------------------------------------71107	2017-11-22-21.12.21.000000	117	Laila	Bird	JUPITER MEDICAL CENTER----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2194,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'71107',
		'Laila',
		'Bird',
		'117',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2194,
		'admitted-on',
		'2017-11-22'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2194,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2194,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2194,
		'hospital-name',
		'JUPITER MEDICAL CENTER'
	);
	
	------------------------------------------71124	2018-09-01-21.02.00.000000	334	Trinity	Hicks	BETHESDA HOSPITAL WEST----------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		2195,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-09-00.00.00.000000',
		'71124',
		'Trinity',
		'Hicks',
		'334',
		'ADT'
	);
	
 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2195,
		'admitted-on',
		'2017-11-22'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2195,
		'event-description',
		'ADMISSION'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2195,
		'event-location',
		'EMERGENCY'
	);

 INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL (
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		2195,
		'hospital-name',
		'BETHESDA HOSPITAL WEST'
	);