
--------------------------------ADDING NEW ED EVENTS-------------------------

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		22557,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-30 10:35:11.974837',
		'12238',
		'A SALAM',
		'HALL',
		'210000365',
		'ADT'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		22558,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-31 10:35:16.534449',
		'12662', 
		'AARON',
		'KUHN',
		'2000000244',
		'ADT'
	);


INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		22559,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-30 10:35:17.439085',
		'10066997', 
		'Jacinta',
		'Kirk',
		'2000000272',
		'ADT'
	);


INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		22560,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-31 10:41:39.280476', 
		'13272',
		'AARIKA',
		'PAULS',
		'210003647',
		'ADT'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		22561,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-29 10:35:13.063971', --2000000275	19097	ABIGAIL	DURHAM
		'15646',
		'AARON',
		'SCHIEBEL',
		'210000636',
		'ADT'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENTS_TBL(
			EVENT_ID,
			EVENT_DOMAIN,
			EVENT_TYPE,
			TRIGGERED_ON_TS,
			MBR_DK,
			MBR_FIRST_NAME,
			MBR_LAST_NAME,
			SOURCE_ID,
			SOURCE_TYPE
		)
	VALUES(
		22562,
		'Transitions in LOC',
		'ed-admission',
		'2018-10-29 10:35:18.302438',
		'15795',
		'AARON',
		'CHIN',
		'210003740',
		'ADT'
	);



--------------------INSERT INTO NEWS FEED DETAILS TBL------------------------
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22557,
		'admitted-on',
		'2018-09-17'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22557,
		'event-description',
		'Admission'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22557,
		'event-location',
		'ED'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22557,
		'hospital-name',
		'JAMES HOSPITAL'
	);
	
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22558,
		'admitted-on',
		'2018-09-17'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22558,
		'event-description',
		'Admission'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22558,
		'event-location',
		'ED'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22558,
		'hospital-name',
		'JAMES HOSPITAL'
	);
	
	
	
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22559,
		'admitted-on',
		'2018-09-17'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22559,
		'event-description',
		'Admission'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22559,
		'event-location',
		'ED'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22559,
		'hospital-name',
		'JAMES HOSPITAL'
	);
	
	
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22560,
		'admitted-on',
		'2018-09-17'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22560,
		'event-description',
		'Admission'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22560,
		'event-location',
		'ED'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22560,
		'hospital-name',
		'JAMES HOSPITAL'
	);
	
	
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22561,
		'admitted-on',
		'2018-09-17'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22561,
		'event-description',
		'Admission'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22561,
		'event-location',
		'ED'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22561,
		'hospital-name',
		'JAMES HOSPITAL'
	);
	


	
INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22562,
		'admitted-on',
		'2018-09-17'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22562,
		'event-description',
		'Admission'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22562,
		'event-location',
		'ED'
	);

INSERT
	INTO
		MENDATA.NEWS_FEED_EVENT_DETAILS_TBL(
			EVENT_ID,
			FIELD_NAME,
			FIELD_VALUE
		)
	VALUES(
		22562,
		'hospital-name',
		'JAMES HOSPITAL'
	);
	
-----------------ATTACHING MEMBERS----------------------------
 ------------------------for testuser1-test---------------------------
UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser1-test',
	CC_EMP_ID = 'testuser1-test1'
WHERE
	MBR_DK IN(
		1,
		11606,
		11947,
		13755,
		15010,
		15068,
		15795,
		15864
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser2-test',
	CC_EMP_ID = 'testuser2-test1'
WHERE
	MBR_DK IN(
		16356,
		16376,
		18158,
		19232,
		19396,
		20072,
		12238
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser3-test',
	CC_EMP_ID = 'testuser3-test1'
WHERE
	MBR_DK IN(
		23401,
		23879,
		24889,
		25033,
		25189,
		25209
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser7-test',
	CC_EMP_ID = 'testuser7-test1'
WHERE
	MBR_DK IN(
		25639,
		25832,
		26677,
		27192,
		27463,
		10066997
	)
	AND ENROLL_STS_CD = 'Active';



UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser8-test',
	CC_EMP_ID = 'testuser8-test1'
WHERE
	MBR_DK IN(
		27948,
		28612,
		28938,
		29723,
		30061,
		31483,
		31484,
		53143
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser9-test',
	CC_EMP_ID = 'testuser9-test1'
WHERE
	MBR_DK IN(
		31496,
		31853,
		34334,
		35127,
		35340,
		36019,
		4017699
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser10-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser10-test',
	CC_EMP_ID = 'testuser10-test1'
WHERE
	MBR_DK IN(
		38124,
		38761,
		39480,
		39805,
		39860,
		41722,
		42585,
		43355,
		1065270
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser11-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser11-test',
	CC_EMP_ID = 'testuser11-test1'
WHERE
	MBR_DK IN(
		44187,
		44709,
		48462,
		48741,
		49615,
		49950,
		7024813
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser12-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser12-test',
	CC_EMP_ID = 'testuser12-test1'
WHERE
	MBR_DK IN(
		50467,
		50668,
		50999,
		51105,
		51157,
		52056,
		52891,
		13272
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for spleshakov-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'spleshakov-test',
	CC_EMP_ID = 'spleshakovtest1'
WHERE
	MBR_DK IN(
		55732,
		56591,
		57198,
		58269,
		60267,
		60365,
		15646
	)
	AND ENROLL_STS_CD = 'Active';


SELECT * FROM 
	MENDATA.MBR_PROG_TBL
WHERE   
	CC_FULL_NM = 'spleshakov-test' AND 
	CC_EMP_ID = 'spleshakovtest1'
AND 
	MBR_DK IN(
		55732,
		56591,
		57198,
		58269,
		60267,
		60365,
		15646
	)
	AND ENROLL_STS_CD = 'Active';

----------------UPDATING EVENTS TO CURRENT for some Member with nmikhailova-----------------
UPDATE
	MENDATA.NEWS_FEED_EVENTS_TBL
SET
	TRIGGERED_ON_TS = '2018-12-09-00.00.01.000000'
WHERE
	mbr_dk IN(
		SELECT
			p.MBR_DK
		FROM
			MENDATA.NEWS_FEED_EVENTS_TBL_1 e
		INNER JOIN MENDATA.MBR_PROG_TBL_2 p ON
			p.MBR_DK = e.MBR_DK
		WHERE
			p.enroll_sts_cd = 'Active'
			AND p.CC_FULL_NM IN(
				'testuser1-test',
				'testuser2-test',
				'testuser3-test',
				'testuser7-test',
				'testuser8-test',
				'testuser9-test',
				'testuser10-test',
				'testuser11-test',
				'testuser12-test',
				'spleshakov-test'
			));
--------------to find ed admission memebers------------------------
CALL MENDATA.RESET_APPLICATION_CACHE();


