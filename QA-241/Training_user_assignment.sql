-----------------ATTACHING MEMBERS----------------------------
 ------------------------for testuser1-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser1-test',
	CC_EMP_ID = 'testuser1-test1'
WHERE
	MBR_DK IN(
		2000530000,
		2000220004
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser2-test',
	CC_EMP_ID = 'testuser2-test1'
WHERE
	MBR_DK IN(
		2000560000,
		2000270004
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser3-test',
	CC_EMP_ID = 'testuser3-test1'
WHERE
	MBR_DK IN(
		2000600000,
		2000300004
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser7-test',
	CC_EMP_ID = 'testuser7-test1'
WHERE
	MBR_DK IN(
		27463,
		2000330004,
		2000330000
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser8-test',
	CC_EMP_ID = 'testuser8-test1'
WHERE
	MBR_DK IN(
		66150,
		2000360004,
		2000390000
	)
	AND ENROLL_STS_CD = 'Active';

UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser9-test',
	CC_EMP_ID = 'testuser9-test1'
WHERE
	MBR_DK IN(
		44294,
		2000400004,
		2000430000
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser10-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser10-test',
	CC_EMP_ID = 'testuser10-test1'
WHERE
	MBR_DK IN(
		49633,
		2000430004,
		2000230000
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser11-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser11-test',
	CC_EMP_ID = 'testuser11-test1'
WHERE
	MBR_DK IN(
		71127,
		2000480004,
		2000050000
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for testuser12-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'testuser12-test',
	CC_EMP_ID = 'testuser12-test1'
WHERE
	MBR_DK IN(
		71107,
		2000520004,
		2000480000
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for spleshakov-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'spleshakov-test',
	CC_EMP_ID = 'spleshakovtest1'
WHERE
	MBR_DK IN(
		2000590004,
		2000220000
	)
	AND ENROLL_STS_CD = 'Active';

------------------------for iknyazyeva---------------------------
 UPDATE
	MENDATA.MBR_PROG_TBL
SET
	CC_FULL_NM = 'iknyazyeva',
	CC_EMP_ID = 'iknyazyeva1'
WHERE
	MBR_DK IN(
		71124,
		2000570004,
		2000150000
	)
	AND ENROLL_STS_CD = 'Active';