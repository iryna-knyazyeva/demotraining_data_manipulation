------------------FOR MAKING ALL MEMBER ACTIVE-------------------

UPDATE
	MENDATA.MBR_DIM
SET
	ACTIVE = '1'
WHERE
	MBR_DK IN(
		1,
		11606,
		11947,
		13755,
		15010,
		15068,
		15795,
		15864,
		16356,
		16376,
		18158,
		19232,
		19396,
		20072,
		12238,
		23401,
		23879,
		24889,
		25033,
		25189,
		25209,
		25639,
		25832,
		26677,
		27192,
		27463,
		10066997,
		27948,
		28612,
		28938,
		29723,
		30061,
		31483,
		31484,
		53143,
		31496,
		31853,
		34334,
		35127,
		35340,
		36019,
		4017699,
		38124,
		38761,
		39480,
		39805,
		39860,
		41722,
		42585,
		43355,
		1065270,
		44187,
		44709,
		48462,
		48741,
		49615,
		49950,
		7024813,
		50467,
		50668,
		50999,
		51105,
		51157,
		52056,
		52891,
		13272,
		55732,
		56591,
		57198,
		58269,
		60267,
		60365,
		15646
	);


UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser1-test',
	CC_EMP_ID = 'testuser1-test1'
WHERE
	MBR_DK IN(
		1,
		11606,
		11947,
		13755,
		15010,
		15068,
		15795,
		15864
	)
	AND ENROLL_STS_CD = 'ACTIVE';

UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser2-test',
	CC_EMP_ID = 'testuser2-test1'
WHERE
	MBR_DK IN(
		16356,
		16376,
		18158,
		19232,
		19396,
		20072,
		12238
	)
	AND ENROLL_STS_CD = 'ACTIVE';

UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser3-test',
	CC_EMP_ID = 'testuser3-test1'
WHERE
	MBR_DK IN(
		23401,
		23879,
		24889,
		25033,
		25189,
		25209
	)
	AND ENROLL_STS_CD = 'ACTIVE';

UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser7-test',
	CC_EMP_ID = 'testuser7-test1'
WHERE
	MBR_DK IN(
		25639,
		25832,
		26677,
		27192,
		27463,
		10066997
	)
	AND ENROLL_STS_CD = 'ACTIVE';



UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser8-test',
	CC_EMP_ID = 'testuser8-test1'
WHERE
	MBR_DK IN(
		27948,
		28612,
		28938,
		29723,
		30061,
		31483,
		31484,
		53143
	)
	AND ENROLL_STS_CD = 'ACTIVE';

UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser9-test',
	CC_EMP_ID = 'testuser9-test1'
WHERE
	MBR_DK IN(
		31496,
		31853,
		34334,
		35127,
		35340,
		36019,
		4017699
	)
	AND ENROLL_STS_CD = 'ACTIVE';

------------------------for testuser10-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser10-test',
	CC_EMP_ID = 'testuser10-test1'
WHERE
	MBR_DK IN(
		38124,
		38761,
		39480,
		39805,
		39860,
		41722,
		42585,
		43355,
		1065270
	)
	AND ENROLL_STS_CD = 'ACTIVE';

------------------------for testuser11-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser11-test',
	CC_EMP_ID = 'testuser11-test1'
WHERE
	MBR_DK IN(
		44187,
		44709,
		48462,
		48741,
		49615,
		49950,
		7024813
	)
	AND ENROLL_STS_CD = 'ACTIVE';

------------------------for testuser12-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'testuser12-test',
	CC_EMP_ID = 'testuser12-test1'
WHERE
	MBR_DK IN(
		50467,
		50668,
		50999,
		51105,
		51157,
		52056,
		52891,
		13272
	)
	AND ENROLL_STS_CD = 'ACTIVE';

------------------------for spleshakov-test---------------------------
 UPDATE
	MENDATA.MBR_PROG_DIM
SET
	CC_FULL_NM = 'spleshakov-test',
	CC_EMP_ID = 'spleshakovtest1'
WHERE
	MBR_DK IN(
		55732,
		56591,
		57198,
		58269,
		60267,
		60365,
		15646
	)
	AND ENROLL_STS_CD = 'ACTIVE';

----------------for member program table update-------------


 CALL MENDATA.REFRESH_MEMBER_LIST_TBL ();
CALL MENDATA.SWAP_ALIASES('MENDATA.MEMBER_LIST_TBL','MENDATA.MEMBER_LIST_TBL_STG',30,30);--
----------------for member list update-------------

SELECT * FROM MENDATA.MEMBER_LIST_TBL_STG WHERE mbr_dk='11606';

SELECT * FROM MENDATA.MEMBER_LIST_TBL WHERE mbr_dk='11606';

CALL MENDATA.RESET_APPLICATION_CACHE();



