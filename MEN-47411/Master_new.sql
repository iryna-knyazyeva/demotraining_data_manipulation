--------------------UPDATE MEMBER_LIST_TBL---------------------
 UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	TIER = '99',
	TIER_LABEL = 'Not Yet Run',
	TIER_DESC = 'New members will receive a tier assignment after the next Member Tier run'
WHERE tier IS NULL;

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	TIER = '1',
	TIER_DESC = 'Members with multiple conditions under active management, high utilization and a recent increase in acute services and/or needs for care management',
	TIER_LABEL = '1 - Complex Care'
WHERE
	MBR_DK IN(
		15020309,
		15044523,
		16001890,
		16006185,
		16038648,
		16064916,
		16065577,
		17001868,
		17005296,
		17009324
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	TIER = '2',
	TIER_DESC = 'Members with multiple conditions, moderately high utilization and an increasing trend for services, medications and/or needs for care management',
	TIER_LABEL = '2 - Emerging Complex'
WHERE
	MBR_DK IN(
		17009594,
		17015230,
		17015818,
		17031466,
		17045122,
		17045909,
		17048121,
		17057097,
		17058293,
		17062576
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	TIER = '3',
	TIER_DESC = 'Members with several conditions apparently under satisfactory control, moderate utilization and stable trend for acute services',
	TIER_LABEL = '3 - Managed'
WHERE
	MBR_DK IN(
		17063813,
		17066288,
		17003610,
		17008552,
		17006082,
		17006683,
		17030114,
		17056362,
		17045933,
		17046696
	);

COMMIT;

UPDATE
	MENDATA.MEMBER_LIST_TBL
SET
	TIER = '4',
	TIER_DESC = 'Members with few if any conditions needing active management, moderate to low utilization and stable community based care',
	TIER_LABEL = '4 - Healthy/Self-Managed'
WHERE
	MBR_DK IN(
		17007837,
		15015189,
		16016585,
		16027991,
		17005998,
		17047220,
		16066751,
		17007551,
		17069215,
		15039581
	);

COMMIT;

--------------------UPDATE MBR_FCT---------------------
 UPDATE
	MENDATA.MBR_FCT
SET
	TIER = '99',
	TIER_LABEL = 'Not Yet Run',
	TIER_DESC = 'New members will receive a tier assignment after the next Member Tier run'
WHERE
	1 = 1;

COMMIT;

UPDATE
	MENDATA.MBR_FCT
SET
	TIER = '1',
	TIER_DESC = 'Members with multiple conditions under active management, high utilization and a recent increase in acute services and/or needs for care management',
	TIER_LABEL = '1 - Complex Care'
WHERE
	MBR_DK IN(
		15020309,
		15044523,
		16001890,
		16006185,
		16038648,
		16064916,
		16065577,
		17001868,
		17005296,
		17009324
	);

COMMIT;

UPDATE
	MENDATA.MBR_FCT
SET
	TIER = '2',
	TIER_DESC = 'Members with multiple conditions, moderately high utilization and an increasing trend for services, medications and/or needs for care management',
	TIER_LABEL = '2 - Emerging Complex'
WHERE
	MBR_DK IN(
		17009594,
		17015230,
		17015818,
		17031466,
		17045122,
		17045909,
		17048121,
		17057097,
		17058293,
		17062576
	);

COMMIT;

UPDATE
	MENDATA.MBR_FCT
SET
	TIER = '3',
	TIER_DESC = 'Members with several conditions apparently under satisfactory control, moderate utilization and stable trend for acute services',
	TIER_LABEL = '3 - Managed'
WHERE
	MBR_DK IN(
		17063813,
		17066288,
		17003610,
		17008552,
		17006082,
		17006683,
		17030114,
		17056362,
		17045933,
		17046696
	);

COMMIT;

UPDATE
	MENDATA.MBR_FCT
SET
	TIER = '4',
	TIER_DESC = 'Members with few if any conditions needing active management, moderate to low utilization and stable community based care',
	TIER_LABEL = '4 - Healthy/Self-Managed'
WHERE
	MBR_DK IN(
		17007837,
		15015189,
		16016585,
		16027991,
		17005998,
		17047220,
		16066751,
		17007551,
		17069215,
		15039581
	);

COMMIT;

CALL MENDATA.RESET_APPLICATION_CACHE();


