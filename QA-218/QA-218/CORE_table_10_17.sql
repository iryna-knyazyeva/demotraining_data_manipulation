-- Members for apply
DECLARE GLOBAL TEMPORARY TABLE SESSION.MBR_LAST_DT(
    MBR_DK INT,
    LAST_ACTIVITY_DT DATE,
    LAST_ACTIVITY_TYPE VARCHAR(2000)
) WITH REPLACE  ON COMMIT PRESERVE ROWS NOT LOGGED ;

--------------------------------------------------
-- Fill Temp table SESSION.MBR_LAST_DT with data
--------------------------------------------------
INSERT INTO SESSION.MBR_LAST_DT(MBR_DK, LAST_ACTIVITY_DT, LAST_ACTIVITY_TYPE)
---------------------------------------
-- START: Plain query from DataSatge
---------------------------------------
SELECT
	MBR_DK,
	LAST_ACTIVITY_DT,
	CAST(
		ACTIVITY_TYPE AS VARCHAR(2000)
	) AS LAST_ACTIVITY_TYPE
FROM
	(
		SELECT
			MBR_DK,
			LAST_ACTIVITY_DT,
			ACTIVITY_TYPE,
			ROW_NUMBER() OVER(
				PARTITION BY MBR_DK
			ORDER BY
				LAST_ACTIVITY_DT DESC
			) AS RN
		FROM
			(
				(
					--Service Claims
 SELECT
						MBR_DK,
						SVC_FROM_DT AS LAST_ACTIVITY_DT,
						'Service: ' || NVL(
							SVC_TYPE_CD_VAL,
							'NA'
						) AS ACTIVITY_TYPE
					FROM
						(
							--Find Max SVC_FROM_DT for each member from Service claim lines
 SELECT
								MBR_DK,
								CLM_NBR,
								CLM_LINE_NBR,
								SVC_TYPE_CD_VAL,
								SVC_FROM_DT,
								ROW_NUMBER() OVER(
									PARTITION BY MBR_DK
								ORDER BY
									SVC_FROM_DT DESC,
									CLM_NBR ASC,
									CLM_LINE_NBR ASC
								) AS RN
							FROM
								MENDATA.CLM_SVC_FCT
							WHERE
								STS_CD = 'RST1'
						)
					WHERE
						RN = 1
				)
		UNION(
				--Pharmacy Claims
 SELECT
					MBR_DK,
					PRSCPTN_START_DT AS LAST_ACTIVITY_DT,
					'Pharmacy: ' || NVL(
						NTNL_DRG_CD,
						'NA'
					) AS ACTIVITY_TYPE
				FROM
					(
						--Find max date for each member in pharmacy claims
 SELECT
							MBR_DK,
							CLM_NBR,
							CLM_LINE_NBR,
							NTNL_DRG_CD,
							PRSCPTN_START_DT,
							ROW_NUMBER() OVER(
								PARTITION BY MBR_DK
							ORDER BY
								PRSCPTN_START_DT DESC,
								CLM_NBR ASC,
								CLM_LINE_NBR ASC
							) AS RN
						FROM
							MENDATA.CLM_PRSCPTN_FCT
						WHERE
							STS_CD = 'RST1'
					)
				WHERE
					RN = 1
			)
	UNION(
			--ADTs
 SELECT
				T1.MBR_DK,
				DATE(T1.REG_DT) AS LAST_ACTIVITY_DT,
				'ADT: ED ADMISSION' AS ACTIVITY_TYPE
			FROM
				(
					--Find max reg date for each member
(
						SELECT
							A.MBR_DK,
							MAX( A.REG_DATE ) AS REG_DT
						FROM
							MENDATA.EVNT_FCT AS E
						JOIN MENDATA.EVNT_TYPE_DIM AS ET ON
							E.EVNT_TYPE_DK = ET.EVNT_TYPE_DK
							AND ET.EVNT_CTGY_TYPE = 'ADT'
						JOIN MENDATA.EVNT_ADT_DETAILS_DIM AS A ON
							E.EVNT_DETAILS_DK = A.EVNT_ADT_DETAILS_DK
						WHERE
							A.EVENT = 'ADMISSION'
							AND A.STS_CD = 'RST1'
						GROUP BY
							A.MBR_DK
					) T1
				LEFT JOIN -- Find ADT associated with the max reg date for each member
(
						SELECT
							*
						FROM
							MENDATA.EVNT_ADT_DETAILS_DIM
						WHERE
							EVENT = 'ADMISSION'
					) EADIM ON
					(
						T1.MBR_DK,
						T1.REG_DT
					)=(
						EADIM.MBR_DK,
						EADIM.REG_DATE
					)
				)
		)
UNION(
		--Prior Auths
 SELECT
			T1.MBR_DK,
			T1.LAST_ACTIVITY_DT,
			'Prior Auth: ' || NVL(
				PADIM.SERVICE_TYPE,
				'NA'
			) AS ACTIVITY_TYPE
		FROM
			(
				--Find max auth start date for each member subset to Approved, service category
(
					SELECT
						PA.MBR_DK,
						MAX( PA.SERV_START_DATE ) AS LAST_ACTIVITY_DT
					FROM
						MENDATA.EVNT_FCT E
					JOIN MENDATA.EVNT_TYPE_DIM ET ON
						E.EVNT_TYPE_DK = ET.EVNT_TYPE_DK
						AND ET.EVNT_CTGY_TYPE = 'Prior Auth'
					JOIN MENDATA.EVNT_PRIOR_AUTH_DETAILS_DIM PA ON
						E.EVNT_DETAILS_DK = PA.EVNT_PRIOR_AUTH_DETAILS_DK
					WHERE
						PA.SERVICE_STATUS = 'A'
						AND PA.SERV_START_DATE <= CURRENT DATE
						AND PA.STS_CD = 'RST1'
					GROUP BY
						PA.MBR_DK
				) T1
			LEFT JOIN -- Find PA associated with the max serv start date for each member
 MENDATA.EVNT_PRIOR_AUTH_DETAILS_DIM PADIM ON
				(
					T1.MBR_DK,
					T1.LAST_ACTIVITY_DT
				)=(
					PADIM.MBR_DK,
					PADIM.SERV_START_DATE
				)
			) WHERE T1.mbr_dk='2000010004'
	)
			)
		WHERE
			mbr_dk IN(
				'2000010005',
				'2000020005',
				'2000030005',
				'2000040005',
				'2000050005',
				'2000060005',
				'2000070005',
				'2000080005',
				'2000090005',
				'2000100005',
				'2000110005',
				'2000120005',
				'2000130005',
				'2000140005',
				'2000150005',
				'2000160005',
				'2000170005',
				'2000180005',
				'2000190005',
				'2000200005',
				'2000210005',
				'2000220005',
				'2000230005',
				'2000240005',
				'2000250005',
				'2000260005',
				'2000270005',
				'2000280005',
				'2000290005',
				'2000300005',
				'2000310005',
				'2000320005',
				'2000330005',
				'2000340005',
				'2000350005'
			)
	)
WHERE
	RN = 1 WITH UR;
-------------------------------------	
-- END: Plain query from DataSatge
-------------------------------------
	
-- Make index
CREATE INDEX SESSION.MBR_LAST_DT_IX ON SESSION.MBR_LAST_DT(MBR_DK, LAST_ACTIVITY_DT);



-----------------------------------------------
-- UPATE MENDATA.MBR_FCT
-----------------------------------------------
MERGE INTO MENDATA.MBR_FCT m
USING
(
	SELECT   
		MBR_DK,  LAST_ACTIVITY_DT, LAST_ACTIVITY_TYPE
 	FROM 
 		SESSION.MBR_LAST_DT 
) MAP ON m.MBR_DK = MAP.MBR_DK
WHEN MATCHED AND m.LAST_ACTIVITY_DT <> MAP.LAST_ACTIVITY_DT THEN UPDATE
SET
	m.LAST_ACTIVITY_DT = MAP.LAST_ACTIVITY_DT,
	m.LAST_ACTIVITY_TYPE = MAP.LAST_ACTIVITY_TYPE
;
-----------------------------------------------
-- UPATE table to fix abhishek issue-------------
-----------------------------------------------
DELETE
FROM
	MENDATA.EVNT_PRIOR_AUTH_DETAILS_DIM
WHERE
	MBR_DK IN(
		'2000010004',
		'2000090004',
		'2000100004',
		'2000210004',
		'2000220004',
		'2000270004',
		'2000310004',
		'2000320004',
		'2000330004',
		'2000350004'
	)
	AND SERV_START_DATE = '2018-10-11';
	
CALL MENDATA.master_refresh();