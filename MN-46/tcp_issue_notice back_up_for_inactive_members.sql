INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010000,
		3757526,
		2000010004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010000,
		3757567,
		2000010000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010000,
		3757590,
		2000010002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010000,
		3795939,
		NULL,
		NULL,
		8862,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010001,
		3795940,
		NULL,
		NULL,
		8863,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010002,
		3758392,
		2000010153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010002,
		3758396,
		2000010154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010002,
		3758397,
		2000010155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000010002,
		3795941,
		NULL,
		NULL,
		8864,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020000,
		3783414,
		2000020002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020000,
		3783412,
		2000020004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020000,
		3783413,
		2000020000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020000,
		3795942,
		NULL,
		NULL,
		8865,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020001,
		3795943,
		NULL,
		NULL,
		8866,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020002,
		3783415,
		2000020153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020002,
		3783416,
		2000020154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020002,
		3783417,
		2000020155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000020002,
		3795944,
		NULL,
		NULL,
		8867,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030000,
		3795945,
		NULL,
		NULL,
		8868,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030000,
		3783419,
		2000030004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030000,
		3783420,
		2000030000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030000,
		3785237,
		2000030002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030001,
		3795946,
		NULL,
		NULL,
		8869,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030002,
		3795947,
		NULL,
		NULL,
		8870,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030002,
		3783421,
		2000030153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030002,
		3783422,
		2000030154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000030002,
		3783423,
		2000030155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040000,
		3783426,
		2000040000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040000,
		3783425,
		2000040004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040000,
		3783427,
		2000040002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040000,
		3795948,
		NULL,
		NULL,
		8871,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040001,
		3795949,
		NULL,
		NULL,
		8872,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040002,
		3783430,
		2000040155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040002,
		3783428,
		2000040153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040002,
		3783429,
		2000040154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000040002,
		3795950,
		NULL,
		NULL,
		8873,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050000,
		3795951,
		NULL,
		NULL,
		8874,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050000,
		3783432,
		2000050004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050000,
		3783433,
		2000050000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050000,
		3783434,
		2000050002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050001,
		3795952,
		NULL,
		NULL,
		8875,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050002,
		3783435,
		2000050153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050002,
		3783436,
		2000050154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050002,
		3783437,
		2000050155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000050002,
		3795953,
		NULL,
		NULL,
		8876,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060000,
		3795954,
		NULL,
		NULL,
		8877,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060000,
		3783439,
		2000060004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060000,
		3783440,
		2000060000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060000,
		3783441,
		2000060002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060001,
		3795955,
		NULL,
		NULL,
		8878,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060002,
		3795956,
		NULL,
		NULL,
		8879,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060002,
		3783443,
		2000060154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060002,
		3783444,
		2000060155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000060002,
		3783442,
		2000060153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070000,
		3795957,
		NULL,
		NULL,
		8880,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070000,
		3783447,
		2000070000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070000,
		3783448,
		2000070002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070000,
		3783446,
		2000070004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070001,
		3795958,
		NULL,
		NULL,
		8881,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070002,
		3783450,
		2000070154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070002,
		3795959,
		NULL,
		NULL,
		8882,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070002,
		3783449,
		2000070153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000070002,
		3783451,
		2000070155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080000,
		3783454,
		2000080000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080000,
		3783455,
		2000080002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080000,
		3783453,
		2000080004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080000,
		3795960,
		NULL,
		NULL,
		8883,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080001,
		3795961,
		NULL,
		NULL,
		8884,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080002,
		3783456,
		2000080153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080002,
		3783458,
		2000080155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080002,
		3783457,
		2000080154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000080002,
		3795962,
		NULL,
		NULL,
		8885,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090000,
		3783460,
		2000090004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090000,
		3795963,
		NULL,
		NULL,
		8886,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090000,
		3783461,
		2000090000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090000,
		3783462,
		2000090002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090001,
		3795964,
		NULL,
		NULL,
		8887,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090002,
		3783463,
		2000090153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090002,
		3783465,
		2000090155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090002,
		3783464,
		2000090154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000090002,
		3795965,
		NULL,
		NULL,
		8888,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100000,
		3795966,
		NULL,
		NULL,
		8889,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100000,
		3783466,
		2000100004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100000,
		3783467,
		2000100000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100000,
		3783468,
		2000100002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100001,
		3795967,
		NULL,
		NULL,
		8890,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100002,
		3783469,
		2000100153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100002,
		3783470,
		2000100154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100002,
		3783471,
		2000100155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000100002,
		3795968,
		NULL,
		NULL,
		8891,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110000,
		3783473,
		2000110004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110000,
		3795969,
		NULL,
		NULL,
		8892,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110000,
		3783475,
		2000110002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110000,
		3783474,
		2000110000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110001,
		3795970,
		NULL,
		NULL,
		8893,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110002,
		3795971,
		NULL,
		NULL,
		8894,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110002,
		3783476,
		2000110153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110002,
		3784785,
		2000110154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000110002,
		3783477,
		2000110155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120000,
		3783479,
		2000120004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120000,
		3783480,
		2000120000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120000,
		3784502,
		2000120002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120000,
		3795972,
		NULL,
		NULL,
		8895,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120001,
		3795973,
		NULL,
		NULL,
		8896,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120002,
		3795974,
		NULL,
		NULL,
		8897,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120002,
		3783481,
		2000120153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120002,
		3783483,
		2000120155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000120002,
		3783482,
		2000120154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130000,
		3795975,
		NULL,
		NULL,
		8898,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130000,
		3783485,
		2000130004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130000,
		3783486,
		2000130000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130000,
		3783487,
		2000130002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130001,
		3795976,
		NULL,
		NULL,
		8899,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130002,
		3783488,
		2000130153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130002,
		3783489,
		2000130154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130002,
		3783490,
		2000130155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000130002,
		3795977,
		NULL,
		NULL,
		8900,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140000,
		3783492,
		2000140004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140000,
		3795978,
		NULL,
		NULL,
		8901,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140000,
		3783493,
		2000140000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140000,
		3783494,
		2000140002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140001,
		3795979,
		NULL,
		NULL,
		8902,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140002,
		3783495,
		2000140153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140002,
		3783496,
		2000140154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140002,
		3783497,
		2000140155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000140002,
		3795980,
		NULL,
		NULL,
		8903,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150000,
		3795981,
		NULL,
		NULL,
		8904,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150000,
		3783499,
		2000150004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150000,
		3783500,
		2000150000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150000,
		3783501,
		2000150002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150001,
		3795982,
		NULL,
		NULL,
		8905,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150002,
		3795983,
		NULL,
		NULL,
		8906,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150002,
		3783502,
		2000150153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150002,
		3783503,
		2000150154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000150002,
		3783504,
		2000150155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160000,
		3783506,
		2000160004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160000,
		3783507,
		2000160000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160000,
		3783508,
		2000160002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160000,
		3795984,
		NULL,
		NULL,
		8907,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160001,
		3795985,
		NULL,
		NULL,
		8908,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160002,
		3795986,
		NULL,
		NULL,
		8909,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160002,
		3783509,
		2000160153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160002,
		3783511,
		2000160155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000160002,
		3783510,
		2000160154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170000,
		3783512,
		2000170004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170000,
		3783513,
		2000170000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170000,
		3783514,
		2000170002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170000,
		3795987,
		NULL,
		NULL,
		8910,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170001,
		3795988,
		NULL,
		NULL,
		8911,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170002,
		3795989,
		NULL,
		NULL,
		8912,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170002,
		3783515,
		2000170153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170002,
		3783516,
		2000170154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000170002,
		3783517,
		2000170155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180000,
		3795990,
		NULL,
		NULL,
		8913,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180000,
		3783519,
		2000180004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180000,
		3783520,
		2000180000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180000,
		3783919,
		2000180002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180001,
		3795991,
		NULL,
		NULL,
		8914,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180002,
		3783522,
		2000180154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180002,
		3783521,
		2000180153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180002,
		3783523,
		2000180155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000180002,
		3795992,
		NULL,
		NULL,
		8915,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190000,
		3795993,
		NULL,
		NULL,
		8916,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190000,
		3783525,
		2000190004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190000,
		3783527,
		2000190002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190000,
		3783526,
		2000190000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190001,
		3795994,
		NULL,
		NULL,
		8917,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190002,
		3783530,
		2000190155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190002,
		3783528,
		2000190153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190002,
		3783529,
		2000190154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000190002,
		3795995,
		NULL,
		NULL,
		8918,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200000,
		3783532,
		2000200004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200000,
		3783534,
		2000200002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200000,
		3783533,
		2000200000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200000,
		3795996,
		NULL,
		NULL,
		8919,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200001,
		3795997,
		NULL,
		NULL,
		8920,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200002,
		3783535,
		2000200153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200002,
		3783536,
		2000200154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200002,
		3783537,
		2000200155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000200002,
		3795998,
		NULL,
		NULL,
		8921,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210000,
		3795999,
		NULL,
		NULL,
		8922,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210000,
		3783538,
		2000210004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210000,
		3783539,
		2000210000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210000,
		3783540,
		2000210002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210001,
		3796000,
		NULL,
		NULL,
		8923,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210002,
		3796001,
		NULL,
		NULL,
		8924,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210002,
		3783542,
		2000210154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210002,
		3783543,
		2000210155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000210002,
		3783541,
		2000210153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220000,
		3783545,
		2000220004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220000,
		3783546,
		2000220000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220000,
		3783547,
		2000220002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220000,
		3796002,
		NULL,
		NULL,
		8925,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220001,
		3796003,
		NULL,
		NULL,
		8926,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220002,
		3796004,
		NULL,
		NULL,
		8927,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220002,
		3783549,
		2000220155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220002,
		3784792,
		2000220154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000220002,
		3783548,
		2000220153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230000,
		3796005,
		NULL,
		NULL,
		8928,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230000,
		3783551,
		2000230004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230000,
		3783552,
		2000230000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230000,
		3784509,
		2000230002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230001,
		3796006,
		NULL,
		NULL,
		8929,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230002,
		3796007,
		NULL,
		NULL,
		8930,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230002,
		3783553,
		2000230153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230002,
		3783554,
		2000230154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000230002,
		3783555,
		2000230155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240000,
		3783557,
		2000240004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240000,
		3783558,
		2000240000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240000,
		3783559,
		2000240002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240000,
		3796008,
		NULL,
		NULL,
		8931,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240001,
		3796009,
		NULL,
		NULL,
		8932,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240002,
		3783560,
		2000240153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240002,
		3783562,
		2000240155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240002,
		3783561,
		2000240154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000240002,
		3796010,
		NULL,
		NULL,
		8933,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250000,
		3796011,
		NULL,
		NULL,
		8934,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250000,
		3783565,
		2000250000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250000,
		3783566,
		2000250002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250000,
		3783564,
		2000250004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250001,
		3796012,
		NULL,
		NULL,
		8935,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250002,
		3783568,
		2000250154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250002,
		3796013,
		NULL,
		NULL,
		8936,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250002,
		3783567,
		2000250153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000250002,
		3783569,
		2000250155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260000,
		3783572,
		2000260000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260000,
		3783573,
		2000260002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260000,
		3783571,
		2000260004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260000,
		3796014,
		NULL,
		NULL,
		8937,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260001,
		3796015,
		NULL,
		NULL,
		8938,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260002,
		3796016,
		NULL,
		NULL,
		8939,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260002,
		3783574,
		2000260153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260002,
		3783575,
		2000260154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000260002,
		3783576,
		2000260155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270000,
		3783579,
		2000270000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270000,
		3783578,
		2000270004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270000,
		3783580,
		2000270002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270000,
		3796017,
		NULL,
		NULL,
		8940,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270001,
		3796018,
		NULL,
		NULL,
		8941,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270002,
		3796019,
		NULL,
		NULL,
		8942,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270002,
		3783581,
		2000270153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270002,
		3783582,
		2000270154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000270002,
		3783583,
		2000270155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280000,
		3783584,
		2000280004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280000,
		3783585,
		2000280000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280000,
		3783586,
		2000280002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280000,
		3796020,
		NULL,
		NULL,
		8943,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280001,
		3796021,
		NULL,
		NULL,
		8944,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280002,
		3783588,
		2000280154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280002,
		3783589,
		2000280155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280002,
		3783587,
		2000280153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000280002,
		3796022,
		NULL,
		NULL,
		8945,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290000,
		3783592,
		2000290000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290000,
		3783591,
		2000290004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290000,
		3783934,
		2000290002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290000,
		3796023,
		NULL,
		NULL,
		8946,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290001,
		3796024,
		NULL,
		NULL,
		8947,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290002,
		3783594,
		2000290154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290002,
		3796025,
		NULL,
		NULL,
		8948,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290002,
		3783593,
		2000290153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000290002,
		3783595,
		2000290155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300000,
		3783597,
		2000300004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300000,
		3783599,
		2000300002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300000,
		3783598,
		2000300000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300000,
		3796026,
		NULL,
		NULL,
		8949,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300001,
		3796027,
		NULL,
		NULL,
		8950,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300002,
		3783602,
		2000300155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300002,
		3783600,
		2000300153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300002,
		3783601,
		2000300154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000300002,
		3796028,
		NULL,
		NULL,
		8951,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310000,
		3783605,
		2000310000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310000,
		3796029,
		NULL,
		NULL,
		8952,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310000,
		3783604,
		2000310004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310000,
		3783606,
		2000310002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310001,
		3796030,
		NULL,
		NULL,
		8953,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310002,
		3796031,
		NULL,
		NULL,
		8954,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310002,
		3783607,
		2000310153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310002,
		3783608,
		2000310154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000310002,
		3783609,
		2000310155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320000,
		3796032,
		NULL,
		NULL,
		8955,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320000,
		3783610,
		2000320004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320000,
		3783611,
		2000320000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320000,
		3783612,
		2000320002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320001,
		3796033,
		NULL,
		NULL,
		8956,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320002,
		3796034,
		NULL,
		NULL,
		8957,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320002,
		3783614,
		2000320154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320002,
		3783615,
		2000320155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000320002,
		3783613,
		2000320153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330000,
		3796035,
		NULL,
		NULL,
		8958,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330000,
		3783617,
		2000330004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330000,
		3783618,
		2000330000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330000,
		3783619,
		2000330002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330001,
		3796036,
		NULL,
		NULL,
		8959,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330002,
		3783621,
		2000330155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330002,
		3784778,
		2000330154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330002,
		3783620,
		2000330153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000330002,
		3796037,
		NULL,
		NULL,
		8960,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340000,
		3783623,
		2000340004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340000,
		3783624,
		2000340000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340000,
		3784497,
		2000340002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340000,
		3796038,
		NULL,
		NULL,
		8961,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340001,
		3796039,
		NULL,
		NULL,
		8962,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340002,
		3796040,
		NULL,
		NULL,
		10157,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340002,
		3783625,
		2000340153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340002,
		3783626,
		2000340154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000340002,
		3783627,
		2000340155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350000,
		3783629,
		2000350004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350000,
		3783630,
		2000350000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350000,
		3783631,
		2000350002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350000,
		3796041,
		NULL,
		NULL,
		10242,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350001,
		3796042,
		NULL,
		NULL,
		10347,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350002,
		3783633,
		2000350154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350002,
		3783634,
		2000350155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350002,
		3783632,
		2000350153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2000350002,
		3796043,
		NULL,
		NULL,
		10415,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001010001,
		3796044,
		NULL,
		NULL,
		10417,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001010002,
		3783635,
		2001010153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001010002,
		3784813,
		2001010154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001010002,
		3796045,
		NULL,
		NULL,
		10418,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001010002,
		3783636,
		2001010155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020000,
		3796046,
		NULL,
		NULL,
		10419,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020000,
		3783638,
		2001020000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020000,
		3783639,
		2001020002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020000,
		3783902,
		2001020004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020001,
		3796047,
		NULL,
		NULL,
		10420,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020002,
		3796048,
		NULL,
		NULL,
		10421,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020002,
		3783641,
		2001020154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020002,
		3783642,
		2001020155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001020002,
		3783640,
		2001020153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030000,
		3783644,
		2001030004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030000,
		3796049,
		NULL,
		NULL,
		10422,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030000,
		3783645,
		2001030000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030000,
		3783646,
		2001030002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030001,
		3796050,
		NULL,
		NULL,
		10423,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030002,
		3796051,
		NULL,
		NULL,
		10424,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030002,
		3783647,
		2001030153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030002,
		3783649,
		2001030155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001030002,
		3783648,
		2001030154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040000,
		3796052,
		NULL,
		NULL,
		10425,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040000,
		3783652,
		2001040000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040000,
		3783653,
		2001040002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040000,
		3783651,
		2001040004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040001,
		3796053,
		NULL,
		NULL,
		10426,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040002,
		3796054,
		NULL,
		NULL,
		10427,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040002,
		3783654,
		2001040153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040002,
		3783655,
		2001040155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001040002,
		3785391,
		2001040154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050000,
		3783656,
		2001050004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050000,
		3796055,
		NULL,
		NULL,
		10428,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050000,
		3783657,
		2001050000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050000,
		3783658,
		2001050002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050001,
		3796056,
		NULL,
		NULL,
		10429,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050002,
		3783659,
		2001050153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050002,
		3783661,
		2001050155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050002,
		3783660,
		2001050154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001050002,
		3796057,
		NULL,
		NULL,
		10430,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060000,
		3796058,
		NULL,
		NULL,
		10431,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060000,
		3783664,
		2001060000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060000,
		3783665,
		2001060002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060000,
		3783663,
		2001060004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060001,
		3796059,
		NULL,
		NULL,
		10432,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060002,
		3783667,
		2001060154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060002,
		3783666,
		2001060153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060002,
		3783668,
		2001060155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001060002,
		3796060,
		NULL,
		NULL,
		10433,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070000,
		3796061,
		NULL,
		NULL,
		10434,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070000,
		3783671,
		2001070000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070000,
		3783672,
		2001070002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070000,
		3783670,
		2001070004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070001,
		3796062,
		NULL,
		NULL,
		10435,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070002,
		3783673,
		2001070153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070002,
		3783675,
		2001070155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070002,
		3783674,
		2001070154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001070002,
		3796063,
		NULL,
		NULL,
		10436,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080000,
		3783678,
		2001080000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080000,
		3796064,
		NULL,
		NULL,
		10437,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080000,
		3783677,
		2001080004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080000,
		3783679,
		2001080002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080001,
		3796065,
		NULL,
		NULL,
		10438,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080002,
		3783682,
		2001080155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080002,
		3783680,
		2001080153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080002,
		3783681,
		2001080154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001080002,
		3796066,
		NULL,
		NULL,
		10439,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090000,
		3796067,
		NULL,
		NULL,
		10440,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090000,
		3783684,
		2001090004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090000,
		3783686,
		2001090002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090000,
		3783685,
		2001090000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090001,
		3796068,
		NULL,
		NULL,
		10441,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090002,
		3783689,
		2001090155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090002,
		3796069,
		NULL,
		NULL,
		10442,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090002,
		3783687,
		2001090153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001090002,
		3783688,
		2001090154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100000,
		3783691,
		2001100004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100000,
		3783692,
		2001100000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100000,
		3783693,
		2001100002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100000,
		3796070,
		NULL,
		NULL,
		10443,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100001,
		3796071,
		NULL,
		NULL,
		10444,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100002,
		3796072,
		NULL,
		NULL,
		10445,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100002,
		3783694,
		2001100153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100002,
		3783695,
		2001100154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001100002,
		3783696,
		2001100155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110000,
		3783698,
		2001110004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110000,
		3783699,
		2001110000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110000,
		3783700,
		2001110002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110000,
		3796073,
		NULL,
		NULL,
		10446,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110001,
		3796074,
		NULL,
		NULL,
		10447,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110002,
		3796075,
		NULL,
		NULL,
		10448,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110002,
		3783702,
		2001110154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110002,
		3783701,
		2001110153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001110002,
		3785043,
		2001110155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120000,
		3783704,
		2001120004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120000,
		3783705,
		2001120000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120000,
		3783706,
		2001120002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120000,
		3796076,
		NULL,
		NULL,
		10449,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120001,
		3796077,
		NULL,
		NULL,
		10450,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120002,
		3796078,
		NULL,
		NULL,
		10451,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120002,
		3783707,
		2001120153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120002,
		3783709,
		2001120155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001120002,
		3783708,
		2001120154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130000,
		3796079,
		NULL,
		NULL,
		10452,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130000,
		3783711,
		2001130004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130000,
		3783713,
		2001130002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130000,
		3783712,
		2001130000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130001,
		3796080,
		NULL,
		NULL,
		10453,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130002,
		3783716,
		2001130155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130002,
		3796081,
		NULL,
		NULL,
		10454,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130002,
		3783714,
		2001130153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001130002,
		3783715,
		2001130154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140000,
		3796082,
		NULL,
		NULL,
		10455,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140000,
		3783718,
		2001140004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140000,
		3783719,
		2001140000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140000,
		3783720,
		2001140002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140001,
		3796083,
		NULL,
		NULL,
		10456,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140002,
		3796084,
		NULL,
		NULL,
		10457,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140002,
		3783721,
		2001140153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140002,
		3783722,
		2001140154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001140002,
		3783723,
		2001140155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150000,
		3796085,
		NULL,
		NULL,
		10458,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150000,
		3783725,
		2001150004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150000,
		3783726,
		2001150000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150000,
		3783727,
		2001150002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150001,
		3796086,
		NULL,
		NULL,
		10459,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150002,
		3783728,
		2001150153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150002,
		3796087,
		NULL,
		NULL,
		10460,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150002,
		3783729,
		2001150154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001150002,
		3785048,
		2001150155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160000,
		3796088,
		NULL,
		NULL,
		10461,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160000,
		3783731,
		2001160004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160000,
		3783733,
		2001160002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160000,
		3783732,
		2001160000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160001,
		3796089,
		NULL,
		NULL,
		10462,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160002,
		3783734,
		2001160153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160002,
		3783735,
		2001160155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160002,
		3796090,
		NULL,
		NULL,
		10463,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001160002,
		3784822,
		2001160154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170000,
		3783903,
		2001170004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170000,
		3796091,
		NULL,
		NULL,
		10464,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170000,
		3783737,
		2001170000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170000,
		3783738,
		2001170002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170001,
		3796092,
		NULL,
		NULL,
		10465,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170002,
		3783739,
		2001170153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170002,
		3783741,
		2001170155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170002,
		3783740,
		2001170154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001170002,
		3796093,
		NULL,
		NULL,
		10466,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180000,
		3783743,
		2001180004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180000,
		3783744,
		2001180000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180000,
		3783745,
		2001180002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180000,
		3796094,
		NULL,
		NULL,
		10467,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180001,
		3796095,
		NULL,
		NULL,
		10468,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180002,
		3796096,
		NULL,
		NULL,
		10469,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180002,
		3783746,
		2001180153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180002,
		3783748,
		2001180155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001180002,
		3783747,
		2001180154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190000,
		3783750,
		2001190004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190000,
		3783751,
		2001190000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190000,
		3783752,
		2001190002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190000,
		3796097,
		NULL,
		NULL,
		10470,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190001,
		3796098,
		NULL,
		NULL,
		10471,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190002,
		3796099,
		NULL,
		NULL,
		10472,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190002,
		3783753,
		2001190153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190002,
		3783754,
		2001190155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001190002,
		3785401,
		2001190154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200000,
		3783757,
		2001200000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200000,
		3783756,
		2001200004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200000,
		3783758,
		2001200002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200000,
		3796100,
		NULL,
		NULL,
		10473,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200001,
		3796101,
		NULL,
		NULL,
		10474,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200002,
		3783759,
		2001200153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200002,
		3796102,
		NULL,
		NULL,
		10475,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200002,
		3783760,
		2001200154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001200002,
		3783761,
		2001200155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210000,
		3796103,
		NULL,
		NULL,
		10476,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210000,
		3783763,
		2001210004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210000,
		3783765,
		2001210002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210000,
		3783764,
		2001210000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210001,
		3796104,
		NULL,
		NULL,
		10477,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210002,
		3783766,
		2001210153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210002,
		3796105,
		NULL,
		NULL,
		10478,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210002,
		3783767,
		2001210155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001210002,
		3785403,
		2001210154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220000,
		3783904,
		2001220002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220000,
		3796106,
		NULL,
		NULL,
		10479,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220000,
		3783768,
		2001220004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220000,
		3783769,
		2001220000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220001,
		3796107,
		NULL,
		NULL,
		10480,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220002,
		3783770,
		2001220153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220002,
		3796108,
		NULL,
		NULL,
		10481,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220002,
		3783771,
		2001220154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001220002,
		3783772,
		2001220155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230000,
		3796109,
		NULL,
		NULL,
		10482,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230000,
		3783775,
		2001230002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230000,
		3783774,
		2001230004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230000,
		3783905,
		2001230000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230001,
		3796110,
		NULL,
		NULL,
		10483,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230002,
		3783777,
		2001230154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230002,
		3783776,
		2001230153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230002,
		3783778,
		2001230155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001230002,
		3796111,
		NULL,
		NULL,
		10484,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240000,
		3796112,
		NULL,
		NULL,
		10485,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240000,
		3783782,
		2001240002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240000,
		3783780,
		2001240004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240000,
		3783781,
		2001240000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240001,
		3796113,
		NULL,
		NULL,
		10486,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240002,
		3783783,
		2001240153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240002,
		3783784,
		2001240154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240002,
		3796114,
		NULL,
		NULL,
		10487,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001240002,
		3783785,
		2001240155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250000,
		3783788,
		2001250000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250000,
		3783787,
		2001250004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250000,
		3796115,
		NULL,
		NULL,
		10488,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250000,
		3783789,
		2001250002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250001,
		3796116,
		NULL,
		NULL,
		10489,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250002,
		3783790,
		2001250153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250002,
		3783791,
		2001250154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250002,
		3783792,
		2001250155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001250002,
		3796117,
		NULL,
		NULL,
		10490,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260000,
		3783794,
		2001260004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260000,
		3796118,
		NULL,
		NULL,
		10491,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260000,
		3783796,
		2001260002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260000,
		3783795,
		2001260000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260001,
		3796119,
		NULL,
		NULL,
		10492,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260002,
		3783799,
		2001260155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260002,
		3796120,
		NULL,
		NULL,
		10493,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260002,
		3783798,
		2001260154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001260002,
		3783797,
		2001260153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270000,
		3762501,
		2001270004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270000,
		3762652,
		2001270000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270000,
		3762708,
		2001270002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270000,
		3796121,
		NULL,
		NULL,
		10494,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270001,
		3796122,
		NULL,
		NULL,
		10495,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270002,
		3796123,
		NULL,
		NULL,
		10496,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270002,
		3763396,
		2001270154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270002,
		3763392,
		2001270153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001270002,
		3763403,
		2001270155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280000,
		3768161,
		2001280002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280000,
		3796124,
		NULL,
		NULL,
		10497,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280000,
		3767896,
		2001280004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280000,
		3768160,
		2001280000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280001,
		3796125,
		NULL,
		NULL,
		10498,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280002,
		3768798,
		2001280153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280002,
		3768799,
		2001280154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280002,
		3768800,
		2001280155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001280002,
		3796126,
		NULL,
		NULL,
		10499,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290000,
		3771823,
		2001290004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290000,
		3771875,
		2001290000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290000,
		3771882,
		2001290002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290000,
		3796127,
		NULL,
		NULL,
		10500,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290001,
		3796128,
		NULL,
		NULL,
		10501,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290002,
		3772076,
		2001290155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290002,
		3772075,
		2001290154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290002,
		3772074,
		2001290153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001290002,
		3796129,
		NULL,
		NULL,
		10502,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300000,
		3796130,
		NULL,
		NULL,
		10503,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300000,
		3772802,
		2001300004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300000,
		3772830,
		2001300000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300000,
		3772838,
		2001300002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300001,
		3796131,
		NULL,
		NULL,
		10504,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300002,
		3773095,
		2001300153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300002,
		3773100,
		2001300154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300002,
		3773101,
		2001300155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001300002,
		3796132,
		NULL,
		NULL,
		10505,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310000,
		3774320,
		2001310002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310000,
		3796133,
		NULL,
		NULL,
		10506,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310000,
		3774245,
		2001310004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310000,
		3774319,
		2001310000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310001,
		3796134,
		NULL,
		NULL,
		10507,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310002,
		3774457,
		2001310153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310002,
		3774458,
		2001310154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310002,
		3784699,
		2001310155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001310002,
		3796135,
		NULL,
		NULL,
		10508,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320000,
		3796136,
		NULL,
		NULL,
		10509,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320000,
		3775617,
		2001320004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320000,
		3775708,
		2001320000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320000,
		3775713,
		2001320002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320001,
		3796137,
		NULL,
		NULL,
		10510,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320002,
		3775946,
		2001320154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320002,
		3775941,
		2001320153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320002,
		3775947,
		2001320155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001320002,
		3796138,
		NULL,
		NULL,
		10511,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330000,
		3776844,
		2001330004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330000,
		3776885,
		2001330000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330000,
		3776887,
		2001330002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330000,
		3796139,
		NULL,
		NULL,
		10512,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330001,
		3796140,
		NULL,
		NULL,
		10513,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330002,
		3796141,
		NULL,
		NULL,
		10514,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330002,
		3777120,
		2001330153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330002,
		3777122,
		2001330155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001330002,
		3777121,
		2001330154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340000,
		3779089,
		2001340000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340000,
		3779090,
		2001340002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340000,
		3796142,
		NULL,
		NULL,
		10515,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340000,
		3778962,
		2001340004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340001,
		3796322,
		NULL,
		NULL,
		10516,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340002,
		3779230,
		2001340154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340002,
		3779231,
		2001340155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340002,
		3796323,
		NULL,
		NULL,
		10517,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001340002,
		3779222,
		2001340153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350000,
		3780129,
		2001350002,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350000,
		3780123,
		2001350000,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350000,
		3796324,
		NULL,
		NULL,
		10518,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350000,
		3784737,
		2001350004,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350001,
		3796325,
		NULL,
		NULL,
		10519,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350002,
		3780518,
		2001350153,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350002,
		3780521,
		2001350154,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350002,
		3796326,
		NULL,
		NULL,
		10520,
		'2018-11-16',
		'10',
		'Transitional Care Period',
		1,
		'Diagnosis',
		'TMLNE',
		180,
		'B',
		'Binary',
		1,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NOTICE_TBL(
			MBR_DK,
			NTFY_FCT_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			NOTICE_CREATE_DATE,
			NOTICE_TYPE,
			NOTICE_NAME,
			THRESHOLD_VALUE,
			CATEGORY_TYPE,
			NOTICE_BEHAVIOR_TYPE,
			NOTICE_DISPLAY_PERIOD_DAYS_COUNT,
			NOTICE_DISPLAY_INFO_TYPE,
			NOTICE_DISPLAY_INFO_TYPE_DESCRIPTION,
			NOTICE_VALUE,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		2001350002,
		3780522,
		2001350155,
		NULL,
		NULL,
		'2018-11-16',
		'08',
		'Psychotic Diagnosis and no Anti-Psychotic Medication',
		1,
		'Diagnosis',
		'PTRN',
		365,
		'B',
		'Binary',
		3,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);
