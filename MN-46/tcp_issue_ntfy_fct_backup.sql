INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796044,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001010001,
		NULL,
		NULL,
		10417,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796067,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001090000,
		NULL,
		NULL,
		10440,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796040,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000340002,
		NULL,
		NULL,
		10157,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796101,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001200001,
		NULL,
		NULL,
		10474,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796112,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001240000,
		NULL,
		NULL,
		10485,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796134,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001310001,
		NULL,
		NULL,
		10507,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3763396,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001270002,
		2001270154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3775617,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001320000,
		2001320004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3774245,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001310000,
		2001310004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3777120,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001330002,
		2001330153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3780123,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001350000,
		2001350000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796046,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001020000,
		NULL,
		NULL,
		10419,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795969,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000110000,
		NULL,
		NULL,
		8892,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3772074,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001290002,
		2001290153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783412,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000020000,
		2000020004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783421,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000030002,
		2000030153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783427,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000040000,
		2000040002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783437,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000050002,
		2000050155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783440,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000060000,
		2000060000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783444,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000060002,
		2000060155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783451,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000070002,
		2000070155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783455,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000080000,
		2000080002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783461,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000090000,
		2000090000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783466,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000100000,
		2000100004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783471,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000100002,
		2000100155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783475,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000110000,
		2000110002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783485,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000130000,
		2000130004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783490,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000130002,
		2000130155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783496,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000140002,
		2000140154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783500,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000150000,
		2000150000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783511,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000160002,
		2000160155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783515,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000170002,
		2000170153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783520,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000180000,
		2000180000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783525,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000190000,
		2000190004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783529,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000190002,
		2000190154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783539,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000210000,
		2000210000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783545,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000220000,
		2000220004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783549,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000220002,
		2000220155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783553,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000230002,
		2000230153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783558,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000240000,
		2000240000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783562,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000240002,
		2000240155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783567,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000250002,
		2000250153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783573,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000260000,
		2000260002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783582,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000270002,
		2000270154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783588,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000280002,
		2000280154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783592,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000290000,
		2000290000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783597,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000300000,
		2000300004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783600,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000300002,
		2000300153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783606,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000310000,
		2000310002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783615,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000320002,
		2000320155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783627,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000340002,
		2000340155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783633,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000350002,
		2000350154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783649,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001030002,
		2001030155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783652,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001040000,
		2001040000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783657,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001050000,
		2001050000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783666,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001060002,
		2001060153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783672,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001070000,
		2001070002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783677,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001080000,
		2001080004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783688,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001090002,
		2001090154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783694,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001100002,
		2001100153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783700,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001110000,
		2001110002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783707,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001120002,
		2001120153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783713,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001130000,
		2001130002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783719,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001140000,
		2001140000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783725,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001150000,
		2001150004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783731,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001160000,
		2001160004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783738,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001170000,
		2001170002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783744,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001180000,
		2001180000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783751,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001190000,
		2001190000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783768,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001220000,
		2001220004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783782,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001240000,
		2001240002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783798,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001260002,
		2001260154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784778,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000330002,
		2000330154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3774319,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001310000,
		2001310000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3775946,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001320002,
		2001320154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783775,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001230000,
		2001230002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3777122,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001330002,
		2001330155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3768799,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001280002,
		2001280154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783419,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000030000,
		2000030004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783441,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000060000,
		2000060002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783488,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000130002,
		2000130153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783504,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000150002,
		2000150155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783538,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000210000,
		2000210004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783554,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000230002,
		2000230154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783574,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000260002,
		2000260153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783621,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000330002,
		2000330155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783638,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001020000,
		2001020000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783691,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001100000,
		2001100004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783706,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001120000,
		2001120002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783726,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001150000,
		2001150000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783746,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001180002,
		2001180153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783787,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001250000,
		2001250004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784502,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000120000,
		2000120002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796045,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001010002,
		NULL,
		NULL,
		10418,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796323,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001340002,
		NULL,
		NULL,
		10517,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3757567,
		'2017-12-06 16:47:07.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000010000,
		2000010000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3757590,
		'2017-12-06 16:47:07.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000010000,
		2000010002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3774457,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001310002,
		2001310153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3774458,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001310002,
		2001310154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3775941,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001320002,
		2001320153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3779090,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001340000,
		2001340002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3780522,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001350002,
		2001350155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783453,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000080000,
		2000080004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783571,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000260000,
		2000260004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783651,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001040000,
		2001040004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783764,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001210000,
		2001210000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783905,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001230000,
		2001230000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3785403,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001210002,
		2001210154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796008,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000240000,
		NULL,
		NULL,
		8931,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796138,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001320002,
		NULL,
		NULL,
		10511,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3773100,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001300002,
		2001300154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783561,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000240002,
		2000240154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783624,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000340000,
		2000340000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783712,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001130000,
		2001130000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783761,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001200002,
		2001200155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783781,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001240000,
		2001240000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795987,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000170000,
		NULL,
		NULL,
		8910,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796012,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000250001,
		NULL,
		NULL,
		8935,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796111,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001230002,
		NULL,
		NULL,
		10484,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795959,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000070002,
		NULL,
		NULL,
		8882,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3780521,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001350002,
		2001350154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796069,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001090002,
		NULL,
		NULL,
		10442,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795947,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000030002,
		NULL,
		NULL,
		8870,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796034,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000320002,
		NULL,
		NULL,
		8957,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796077,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001120001,
		NULL,
		NULL,
		10450,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796142,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001340000,
		NULL,
		NULL,
		10515,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3763392,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001270002,
		2001270153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3763403,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001270002,
		2001270155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3768160,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001280000,
		2001280000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3778962,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001340000,
		2001340004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3762652,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001270000,
		2001270000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795977,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000130002,
		NULL,
		NULL,
		8900,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796041,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000350000,
		NULL,
		NULL,
		10242,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3768161,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001280000,
		2001280002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3771875,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001290000,
		2001290000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783473,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000110000,
		2000110004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783678,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001080000,
		2001080000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783716,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001130002,
		2001130155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783743,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001180000,
		2001180004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783777,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001230002,
		2001230154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783794,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001260000,
		2001260004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796119,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001260001,
		NULL,
		NULL,
		10492,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796118,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001260000,
		NULL,
		NULL,
		10491,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783414,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000020000,
		2000020002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783432,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000050000,
		2000050004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783435,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000050002,
		2000050153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783436,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000050002,
		2000050154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783448,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000070000,
		2000070002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783463,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000090002,
		2000090153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783465,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000090002,
		2000090155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783467,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000100000,
		2000100000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783480,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000120000,
		2000120000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783483,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000120002,
		2000120155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783493,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000140000,
		2000140000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3776844,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001330000,
		2001330004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783508,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000160000,
		2000160002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783512,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000170000,
		2000170004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783517,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000170002,
		2000170155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783528,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000190002,
		2000190153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783532,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000200000,
		2000200004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783540,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000210000,
		2000210002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783543,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000210002,
		2000210155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783559,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000240000,
		2000240002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783569,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000250002,
		2000250155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783572,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000260000,
		2000260000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783580,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000270000,
		2000270002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783591,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000290000,
		2000290004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783593,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000290002,
		2000290153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783604,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000310000,
		2000310004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783611,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000320000,
		2000320000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3779231,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001340002,
		2001340155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796085,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001150000,
		NULL,
		NULL,
		10458,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796106,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001220000,
		NULL,
		NULL,
		10479,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796126,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001280002,
		NULL,
		NULL,
		10499,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796005,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000230000,
		NULL,
		NULL,
		8928,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783619,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000330000,
		2000330002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783625,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000340002,
		2000340153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783634,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000350002,
		2000350155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783642,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001020002,
		2001020155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783653,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001040000,
		2001040002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783665,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001060000,
		2001060002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783675,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001070002,
		2001070155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783680,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001080002,
		2001080153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783695,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001100002,
		2001100154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783696,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001100002,
		2001100155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795975,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000130000,
		NULL,
		NULL,
		8898,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783709,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001120002,
		2001120155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783718,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001140000,
		2001140004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783721,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001140002,
		2001140153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783729,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001150002,
		2001150154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783737,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001170000,
		2001170000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783765,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001210000,
		2001210002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783902,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001020000,
		2001020004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783425,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000040000,
		2000040004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783428,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000040002,
		2000040153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783433,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000050000,
		2000050000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783449,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000070002,
		2000070153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783454,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000080000,
		2000080000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783462,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000090000,
		2000090002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783469,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000100002,
		2000100153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783476,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000110002,
		2000110153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783479,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000120000,
		2000120004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783481,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000120002,
		2000120153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783486,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000130000,
		2000130000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783489,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000130002,
		2000130154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783495,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000140002,
		2000140153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783499,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000150000,
		2000150004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783507,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000160000,
		2000160000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783513,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000170000,
		2000170000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783519,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000180000,
		2000180004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783521,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000180002,
		2000180153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783523,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000180002,
		2000180155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783527,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000190000,
		2000190002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783535,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000200002,
		2000200153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783542,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000210002,
		2000210154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783546,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000220000,
		2000220000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783547,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000220000,
		2000220002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783557,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000240000,
		2000240004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783565,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000250000,
		2000250000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783566,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000250000,
		2000250002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783585,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000280000,
		2000280000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783586,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000280000,
		2000280002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783589,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000280002,
		2000280155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783599,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000300000,
		2000300002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783601,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000300002,
		2000300154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783607,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000310002,
		2000310153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783612,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000320000,
		2000320002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783617,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000330000,
		2000330004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783618,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000330000,
		2000330000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783623,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000340000,
		2000340004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783630,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000350000,
		2000350000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783631,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000350000,
		2000350002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783641,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001020002,
		2001020154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783645,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001030000,
		2001030000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783647,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001030002,
		2001030153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783655,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001040002,
		2001040155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783658,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001050000,
		2001050002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783664,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001060000,
		2001060000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783673,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001070002,
		2001070153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784785,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000110002,
		2000110154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783679,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001080000,
		2001080002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783684,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001090000,
		2001090004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783687,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001090002,
		2001090153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783698,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001110000,
		2001110004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783699,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001110000,
		2001110000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783702,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001110002,
		2001110154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783714,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001130002,
		2001130153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783723,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001140002,
		2001140155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783733,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001160000,
		2001160002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783735,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001160002,
		2001160155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783739,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001170002,
		2001170153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783745,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001180000,
		2001180002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783754,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001190002,
		2001190155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783763,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001210000,
		2001210004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783771,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001220002,
		2001220154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783776,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001230002,
		2001230153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783796,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001260000,
		2001260002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796020,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000280000,
		NULL,
		NULL,
		8943,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3772076,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001290002,
		2001290155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3779089,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001340000,
		2001340000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3758396,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000010002,
		2000010154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3758397,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000010002,
		2000010155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3758392,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000010002,
		2000010153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783753,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001190002,
		2001190153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783682,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001080002,
		2001080155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3762708,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001270000,
		2001270002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3762501,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001270000,
		2001270004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3772075,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001290002,
		2001290154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3779230,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001340002,
		2001340154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783602,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000300002,
		2000300155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783757,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001200000,
		2001200000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783904,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001220000,
		2001220002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3768798,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001280002,
		2001280153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3772802,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001300000,
		2001300004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3772830,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001300000,
		2001300000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3772838,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001300000,
		2001300002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3780518,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001350002,
		2001350153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783416,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000020002,
		2000020154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783422,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000030002,
		2000030154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783439,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000060000,
		2000060004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783443,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000060002,
		2000060154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783458,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000080002,
		2000080155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783474,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000110000,
		2000110000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783503,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000150002,
		2000150154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783516,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000170002,
		2000170154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783536,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000200002,
		2000200154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783555,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000230002,
		2000230155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783578,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000270000,
		2000270004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783581,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000270002,
		2000270153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783595,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000290002,
		2000290155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783610,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000320000,
		2000320004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783614,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000320002,
		2000320154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783629,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000350000,
		2000350004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783639,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001020000,
		2001020002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783646,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001030000,
		2001030002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783659,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001050002,
		2001050153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783671,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001070000,
		2001070000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783681,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001080002,
		2001080154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783705,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001120000,
		2001120000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783715,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001130002,
		2001130154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783727,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001150000,
		2001150002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783734,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001160002,
		2001160153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783760,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001200002,
		2001200154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3773101,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001300002,
		2001300155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3775947,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001320002,
		2001320155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783413,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000020000,
		2000020000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783442,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000060002,
		2000060153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783446,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000070000,
		2000070004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783477,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000110002,
		2000110155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783482,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000120002,
		2000120154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783487,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000130000,
		2000130002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783514,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000170000,
		2000170002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783533,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000200000,
		2000200000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783541,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000210002,
		2000210153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783564,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000250000,
		2000250004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783598,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000300000,
		2000300000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783609,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000310002,
		2000310155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783613,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000320002,
		2000320153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783636,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001010002,
		2001010155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783640,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001020002,
		2001020153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783660,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001050002,
		2001050154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783670,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001070000,
		2001070004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783720,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001140000,
		2001140002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783740,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001170002,
		2001170154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783747,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001180002,
		2001180154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783767,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001210002,
		2001210155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783769,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001220000,
		2001220000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783778,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001230002,
		2001230155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783789,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001250000,
		2001250002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783791,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001250002,
		2001250154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783792,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001250002,
		2001250155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783919,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000180000,
		2000180002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783934,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000290000,
		2000290002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3785401,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001190002,
		2001190154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3771882,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001290000,
		2001290002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3773095,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001300002,
		2001300153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3775713,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001320000,
		2001320002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783526,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000190000,
		2000190000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783548,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000220002,
		2000220153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783701,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001110002,
		2001110153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783780,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001240000,
		2001240004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783795,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001260000,
		2001260000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795939,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000010000,
		NULL,
		NULL,
		8862,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796010,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000240002,
		NULL,
		NULL,
		8933,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796066,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001080002,
		NULL,
		NULL,
		10439,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796114,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001240002,
		NULL,
		NULL,
		10487,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795955,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000060001,
		NULL,
		NULL,
		8878,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795976,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000130001,
		NULL,
		NULL,
		8899,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796026,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000300000,
		NULL,
		NULL,
		8949,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796053,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001040001,
		NULL,
		NULL,
		10426,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796107,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001220001,
		NULL,
		NULL,
		10480,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795948,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000040000,
		NULL,
		NULL,
		8871,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795956,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000060002,
		NULL,
		NULL,
		8879,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795964,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000090001,
		NULL,
		NULL,
		8887,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795971,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000110002,
		NULL,
		NULL,
		8894,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795978,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000140000,
		NULL,
		NULL,
		8901,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795983,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000150002,
		NULL,
		NULL,
		8906,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795989,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000170002,
		NULL,
		NULL,
		8912,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795994,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000190001,
		NULL,
		NULL,
		8917,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795997,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000200001,
		NULL,
		NULL,
		8920,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796016,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000260002,
		NULL,
		NULL,
		8939,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796023,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000290000,
		NULL,
		NULL,
		8946,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796029,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000310000,
		NULL,
		NULL,
		8952,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796035,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000330000,
		NULL,
		NULL,
		8958,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796038,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000340000,
		NULL,
		NULL,
		8961,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796048,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001020002,
		NULL,
		NULL,
		10421,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796056,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001050001,
		NULL,
		NULL,
		10429,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796058,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001060000,
		NULL,
		NULL,
		10431,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796065,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001080001,
		NULL,
		NULL,
		10438,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796073,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001110000,
		NULL,
		NULL,
		10446,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796076,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001120000,
		NULL,
		NULL,
		10449,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796086,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001150001,
		NULL,
		NULL,
		10459,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796092,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001170001,
		NULL,
		NULL,
		10465,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796096,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001180002,
		NULL,
		NULL,
		10469,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796098,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001190001,
		NULL,
		NULL,
		10471,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796102,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001200002,
		NULL,
		NULL,
		10475,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796104,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001210001,
		NULL,
		NULL,
		10477,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796115,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001250000,
		NULL,
		NULL,
		10488,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796122,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001270001,
		NULL,
		NULL,
		10495,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796127,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001290000,
		NULL,
		NULL,
		10500,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796136,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001320000,
		NULL,
		NULL,
		10509,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796141,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001330002,
		NULL,
		NULL,
		10514,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796326,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001350002,
		NULL,
		NULL,
		10520,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3767896,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001280000,
		2001280004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3776885,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001330000,
		2001330000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783785,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001240002,
		2001240155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784497,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000340000,
		2000340002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796006,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000230001,
		NULL,
		NULL,
		8929,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3776887,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001330000,
		2001330002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3777121,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001330002,
		2001330154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3785043,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001110002,
		2001110155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3785391,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001040002,
		2001040154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795944,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000020002,
		NULL,
		NULL,
		8867,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796080,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001130001,
		NULL,
		NULL,
		10453,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795952,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000050001,
		NULL,
		NULL,
		8875,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783415,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000020002,
		2000020153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783460,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000090000,
		2000090004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783568,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000250002,
		2000250154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783579,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000270000,
		2000270000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783656,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001050000,
		2001050004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783728,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001150002,
		2001150153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783759,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001200002,
		2001200153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783788,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001250000,
		2001250000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783799,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001260002,
		2001260155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783903,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001170000,
		2001170004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796013,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000250002,
		NULL,
		NULL,
		8936,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796061,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001070000,
		NULL,
		NULL,
		10434,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796088,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001160000,
		NULL,
		NULL,
		10461,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796131,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001300001,
		NULL,
		NULL,
		10504,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796021,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000280001,
		NULL,
		NULL,
		8944,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784509,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000230000,
		2000230002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784737,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001350000,
		2001350004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784822,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001160002,
		2001160154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795957,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000070000,
		NULL,
		NULL,
		8880,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796007,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000230002,
		NULL,
		NULL,
		8930,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796052,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001040000,
		NULL,
		NULL,
		10425,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796068,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001090001,
		NULL,
		NULL,
		10441,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796074,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001110001,
		NULL,
		NULL,
		10447,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796079,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001130000,
		NULL,
		NULL,
		10452,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796087,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001150002,
		NULL,
		NULL,
		10460,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796113,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001240001,
		NULL,
		NULL,
		10486,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796123,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001270002,
		NULL,
		NULL,
		10496,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796140,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001330001,
		NULL,
		NULL,
		10513,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795941,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000010002,
		NULL,
		NULL,
		8864,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795943,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000020001,
		NULL,
		NULL,
		8866,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796033,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000320001,
		NULL,
		NULL,
		8956,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796050,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001030001,
		NULL,
		NULL,
		10423,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796060,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001060002,
		NULL,
		NULL,
		10433,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796135,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001310002,
		NULL,
		NULL,
		10508,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796014,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000260000,
		NULL,
		NULL,
		8937,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796047,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001020001,
		NULL,
		NULL,
		10420,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796325,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001350001,
		NULL,
		NULL,
		10519,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795945,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000030000,
		NULL,
		NULL,
		8868,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795949,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000040001,
		NULL,
		NULL,
		8872,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795951,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000050000,
		NULL,
		NULL,
		8874,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795954,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000060000,
		NULL,
		NULL,
		8877,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795961,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000080001,
		NULL,
		NULL,
		8884,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795963,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000090000,
		NULL,
		NULL,
		8886,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795965,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000090002,
		NULL,
		NULL,
		8888,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795966,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000100000,
		NULL,
		NULL,
		8889,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795970,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000110001,
		NULL,
		NULL,
		8893,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795973,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000120001,
		NULL,
		NULL,
		8896,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795979,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000140001,
		NULL,
		NULL,
		8902,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795980,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000140002,
		NULL,
		NULL,
		8903,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795981,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000150000,
		NULL,
		NULL,
		8904,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795982,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000150001,
		NULL,
		NULL,
		8905,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795984,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000160000,
		NULL,
		NULL,
		8907,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795985,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000160001,
		NULL,
		NULL,
		8908,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795992,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000180002,
		NULL,
		NULL,
		8915,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795996,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000200000,
		NULL,
		NULL,
		8919,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796000,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000210001,
		NULL,
		NULL,
		8923,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796004,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000220002,
		NULL,
		NULL,
		8927,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796011,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000250000,
		NULL,
		NULL,
		8934,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796017,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000270000,
		NULL,
		NULL,
		8940,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796024,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000290001,
		NULL,
		NULL,
		8947,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796027,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000300001,
		NULL,
		NULL,
		8950,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796028,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000300002,
		NULL,
		NULL,
		8951,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796031,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000310002,
		NULL,
		NULL,
		8954,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796032,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000320000,
		NULL,
		NULL,
		8955,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796039,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000340001,
		NULL,
		NULL,
		8962,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796042,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000350001,
		NULL,
		NULL,
		10347,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796055,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001050000,
		NULL,
		NULL,
		10428,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796057,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001050002,
		NULL,
		NULL,
		10430,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796059,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001060001,
		NULL,
		NULL,
		10432,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796062,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001070001,
		NULL,
		NULL,
		10435,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796064,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001080000,
		NULL,
		NULL,
		10437,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796071,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001100001,
		NULL,
		NULL,
		10444,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796075,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001110002,
		NULL,
		NULL,
		10448,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796084,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001140002,
		NULL,
		NULL,
		10457,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796089,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001160001,
		NULL,
		NULL,
		10462,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796094,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001180000,
		NULL,
		NULL,
		10467,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796095,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001180001,
		NULL,
		NULL,
		10468,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796099,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001190002,
		NULL,
		NULL,
		10472,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796100,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001200000,
		NULL,
		NULL,
		10473,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796103,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001210000,
		NULL,
		NULL,
		10476,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796105,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001210002,
		NULL,
		NULL,
		10478,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796108,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001220002,
		NULL,
		NULL,
		10481,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796116,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001250001,
		NULL,
		NULL,
		10489,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796117,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001250002,
		NULL,
		NULL,
		10490,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796120,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001260002,
		NULL,
		NULL,
		10493,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796121,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001270000,
		NULL,
		NULL,
		10494,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796124,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001280000,
		NULL,
		NULL,
		10497,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796130,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001300000,
		NULL,
		NULL,
		10503,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795946,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000030001,
		NULL,
		NULL,
		8869,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796009,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000240001,
		NULL,
		NULL,
		8932,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796078,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001120002,
		NULL,
		NULL,
		10451,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796132,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001300002,
		NULL,
		NULL,
		10505,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783506,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000160000,
		2000160004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783594,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000290002,
		2000290154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783644,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001030000,
		2001030004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796001,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000210002,
		NULL,
		NULL,
		8924,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796036,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000330001,
		NULL,
		NULL,
		8959,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796109,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001230000,
		NULL,
		NULL,
		10482,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796019,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000270002,
		NULL,
		NULL,
		8942,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796133,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001310000,
		NULL,
		NULL,
		10506,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796137,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001320001,
		NULL,
		NULL,
		10510,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796139,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001330000,
		NULL,
		NULL,
		10512,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796322,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001340001,
		NULL,
		NULL,
		10516,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784699,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001310002,
		2001310155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3785048,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001150002,
		2001150155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795968,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000100002,
		NULL,
		NULL,
		8891,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795991,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000180001,
		NULL,
		NULL,
		8914,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796070,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001100000,
		NULL,
		NULL,
		10443,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796129,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001290002,
		NULL,
		NULL,
		10502,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795940,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000010001,
		NULL,
		NULL,
		8863,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795942,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000020000,
		NULL,
		NULL,
		8865,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795958,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000070001,
		NULL,
		NULL,
		8881,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795962,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000080002,
		NULL,
		NULL,
		8885,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795972,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000120000,
		NULL,
		NULL,
		8895,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796002,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000220000,
		NULL,
		NULL,
		8925,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796018,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000270001,
		NULL,
		NULL,
		8941,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796030,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000310001,
		NULL,
		NULL,
		8953,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796063,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001070002,
		NULL,
		NULL,
		10436,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796097,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001190000,
		NULL,
		NULL,
		10470,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796110,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001230001,
		NULL,
		NULL,
		10483,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795993,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000190000,
		NULL,
		NULL,
		8916,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796072,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001100002,
		NULL,
		NULL,
		10445,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796081,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001130002,
		NULL,
		NULL,
		10454,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796324,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001350000,
		NULL,
		NULL,
		10518,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3771823,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001290000,
		2001290004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3774320,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001310000,
		2001310002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783426,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000040000,
		2000040000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783492,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000140000,
		2000140004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783605,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000310000,
		2000310000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783689,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001090002,
		2001090155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783704,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001120000,
		2001120004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783770,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001220002,
		2001220153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783783,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001240002,
		2001240153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796125,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001280001,
		NULL,
		NULL,
		10498,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795990,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000180000,
		NULL,
		NULL,
		8913,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796049,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001030000,
		NULL,
		NULL,
		10422,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796082,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001140000,
		NULL,
		NULL,
		10455,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796091,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001170000,
		NULL,
		NULL,
		10464,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3768800,
		'2017-12-06 16:47:08.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001280002,
		2001280155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3775708,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001320000,
		2001320000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795999,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000210000,
		NULL,
		NULL,
		8922,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795986,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000160002,
		NULL,
		NULL,
		8909,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783417,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000020002,
		2000020155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783429,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000040002,
		2000040154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783447,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000070000,
		2000070000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783456,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000080002,
		2000080153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783470,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000100002,
		2000100154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783494,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000140000,
		2000140002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783502,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000150002,
		2000150153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783509,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000160002,
		2000160153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783534,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000200000,
		2000200002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783551,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000230000,
		2000230004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783560,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000240002,
		2000240153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783575,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000260002,
		2000260154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783584,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000280000,
		2000280004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783608,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000310002,
		2000310154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783626,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000340002,
		2000340154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783635,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001010002,
		2001010153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783654,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001040002,
		2001040153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783661,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001050002,
		2001050155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783668,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001060002,
		2001060155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783686,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001090000,
		2001090002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783692,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001100000,
		2001100000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783711,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001130000,
		2001130004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783722,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001140002,
		2001140154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783741,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001170002,
		2001170155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783748,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001180002,
		2001180155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783790,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001250002,
		2001250153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784792,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000220002,
		2000220154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3784813,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001010002,
		2001010154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3757526,
		'2017-12-06 16:47:07.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000010000,
		2000010004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3779222,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001340002,
		2001340153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783423,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000030002,
		2000030155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783468,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000100000,
		2000100002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783501,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000150000,
		2000150002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783537,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000200002,
		2000200155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783552,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000230000,
		2000230000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783587,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000280002,
		2000280153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783632,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000350002,
		2000350153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783648,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001030002,
		2001030154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783693,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001100000,
		2001100002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783774,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001230000,
		2001230004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796022,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000280002,
		NULL,
		NULL,
		8945,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796090,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001160002,
		NULL,
		NULL,
		10463,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796037,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000330002,
		NULL,
		NULL,
		8960,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795953,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000050002,
		NULL,
		NULL,
		8876,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795988,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000170001,
		NULL,
		NULL,
		8911,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795967,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000100001,
		NULL,
		NULL,
		8890,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795995,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000190002,
		NULL,
		NULL,
		8918,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796025,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000290002,
		NULL,
		NULL,
		8948,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796043,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000350002,
		NULL,
		NULL,
		10415,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796083,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001140001,
		NULL,
		NULL,
		10456,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3780129,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001350000,
		2001350002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783450,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000070002,
		2000070154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783530,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000190002,
		2000190155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783667,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001060002,
		2001060154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783750,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001190000,
		2001190004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783766,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001210002,
		2001210153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795974,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000120002,
		NULL,
		NULL,
		8897,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796051,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001030002,
		NULL,
		NULL,
		10424,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796054,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001040002,
		NULL,
		NULL,
		10427,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796015,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000260001,
		NULL,
		NULL,
		8938,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783430,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000040002,
		2000040155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783522,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000180002,
		2000180154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783420,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000030000,
		2000030000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783434,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000050000,
		2000050002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783457,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000080002,
		2000080154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783464,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000090002,
		2000090154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783497,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000140002,
		2000140155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783510,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000160002,
		2000160154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783576,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000260002,
		2000260155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783583,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000270002,
		2000270155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783620,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000330002,
		2000330153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783663,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001060000,
		2001060004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783674,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001070002,
		2001070154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783685,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001090000,
		2001090000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783708,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001120002,
		2001120154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783732,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001160000,
		2001160000,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783752,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001190000,
		2001190002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783756,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001200000,
		2001200004,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783758,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001200000,
		2001200002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783772,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001220002,
		2001220155,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783784,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001240002,
		2001240154,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3783797,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2001260002,
		2001260153,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 03/07/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3785237,
		'2017-12-06 16:47:09.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		8,
		2000030000,
		2000030002,
		NULL,
		NULL,
		'No anti-psychotic prescriptions within the last 3 months',
		'3 services with psychotic diagnosis within the last 12 months with last service on 05/20/2017. No anti-psychotic prescriptions within the last 3 months',
		'Psychotic Diagnosis and no Anti-Psychotic Medication. Review for need to add medication treatment for condition'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795950,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000040002,
		NULL,
		NULL,
		8873,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795960,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000080000,
		NULL,
		NULL,
		8883,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3795998,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000200002,
		NULL,
		NULL,
		8921,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796003,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2000220001,
		NULL,
		NULL,
		8926,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796093,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001170002,
		NULL,
		NULL,
		10466,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);

INSERT
	INTO
		MENDATA.NTFY_FCT(
			NTFY_FCT_DK,
			VLD_FROM_TS,
			VLD_TO_TS,
			REC_CRT_DT,
			REC_LAST_UPD_DT,
			NTFY_TYPE_DK,
			MBR_DK,
			CLM_SVC_DK,
			CLM_PRSCPTN_DK,
			EVNT_FCT_DK,
			SHORT_MESSAGE,
			DETAILED_MESSAGE,
			INSTRUCTION
		)
	VALUES(
		3796128,
		'2017-12-06 16:47:10.000000',
		'9999-12-31 00:00:00.000000',
		'2018-11-16',
		'2018-11-16',
		10,
		2001290001,
		NULL,
		NULL,
		10501,
		'ER Hospital Visit occurred on 11-15-2018',
		'ER Hospital Visit occurred on 11-15-2018 and no follow up visit has occurred',
		'HEDIS Alert: 7 and 30 Day Follow-up Needed. Ensure follow up appointments within 3 days of discharge. Ensure member has a PCP visit within 5-7 days of discharge. Ensure member has filled discharge medications within 24-48 hours of discharge. Initiate Transition of Care (TOC)/Discharge Follow-Up assessment within 3 days'
	);
